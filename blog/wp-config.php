<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cakraapp_blogcakra');

/** MySQL database username */
define('DB_USER', 'cakraapp_blog');

/** MySQL database password */
define('DB_PASSWORD', 'blogcakra');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';L=jf!e3*f=b|v;PIqD~D:xc6e>~a&D>1eiv v+Y_I#OVK6&!!+:j203p}}!L;(=');
define('SECURE_AUTH_KEY',  'TM2E(=zhQi6NJ9iZiN:!2$$(kOYi=+;b56YLcUH4y04IB(Ch5jMdK8ksQkDmsX9y');
define('LOGGED_IN_KEY',    'BtS4nL+J9l&HvkWDGX0N(C>WJ.mA[_[Z>X|Cp`zW+ds&<<J]=@6KF*1)J!!G#WH8');
define('NONCE_KEY',        'a1<^$:Cr*3YIMi%nN}4Xzb:`|@w?^*.wY@OeP,<&8[wRnjg 4MkQwo[/O97Jj}[W');
define('AUTH_SALT',        '0HYnO~JT@c>&~K?I(z:EqQ2 G)mSIFxRJ]iIiKM!j#02>D#D* {2q`c(`m^QU1=U');
define('SECURE_AUTH_SALT', '{]eb3: KF9ywM:V&Ja=5HYrl4_a=SqA%C2V=l~yA1C67an]s4hH1vzCTbE?Oguv(');
define('LOGGED_IN_SALT',   'd::DuL]h^m8Wp.!IXwD*NU-_l(sM1NFGvyNqjpeEWlpuf{5o$_Q2lD&cfxue)g`b');
define('NONCE_SALT',       '>9-oq~(@2[X3Ju-=X]fDlAP]x)^T.~VizO^=S&t*9k]`;-h:ZuZ*?y>|P9i2otbO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'blogcakra';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
