<?php get_header();?>

	<body lang="en" class="single "> 
		<?php if (have_posts()) : ?>
   			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
				<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>
    
		<?php while (have_posts()) : the_post(); ?>
			<?php if ( !is_single() ) :
					get_template_part ('content',get_post_format);?>
 			<?php else:
    				get_template_part ('singlePost',get_post_format);?>
			<?php endif;?>
			<?php if ( comments_open() || get_comments_number() ) {
						comments_template();}?>
		<?php endwhile;?>
            <?php pagination_number(); ?>
		<?php else :
			get_template_part( 'content', 'none' );
	endif;
	?>
        
<?php get_footer();?>