	<footer class="site-footer">
    
		<div class="site-nav">
			<?php
			$args = array(
				'theme_location' => 'footer'
			);
			?>
			<?php wp_nav_menu(); ?>	
		</div>

	</footer>
	<br>
	</br>
        	
	</div>
    <div class= "copy"><?php echo('Copyright'); ?> &copy; <?php echo date('Y');?> <?php echo('MRH Studio. All rights reserved.'); ?></div>
	<?php wp_footer();?>
	</body>
</html>