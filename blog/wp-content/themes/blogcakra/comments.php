<?php

if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>

	<h2 class="comments-title">
		<?php
			printf( _n( '1 komentar pada &ldquo;%2$s&rdquo;', '%1$s komentar pada &ldquo;%2$s&rdquo;', get_comments_number() ),
				number_format_i18n( get_comments_number() ), get_the_title() );
		?>
	</h2>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Comment navigation' ); ?></h1>
		<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;' ) ); ?></div>
	</nav><!-- #comment-nav-above -->
	<?php endif; // Check for comment navigation. ?>

	<ol class="comment-list">
		<?php
			wp_list_comments( array(
				'style'       => 'ol',
				'short_ping'  => true,
				'avatar_size' => 34,
			) );
		?>
	</ol><!-- .comment-list -->

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Comment navigation'); ?></h1>
		<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;' ) ); ?></div>
	</nav><!-- #comment-nav-below -->
	<?php endif; // Check for comment navigation. ?>

	<?php if ( ! comments_open() ) : ?>
	<p class="no-comments"><?php _e( 'Comments are closed.' ); ?></p>
	<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php $comment_args = array( 'title_reply'=>'Berikan Komentar Anda pada Artikel Ini:',

'fields' => apply_filters( 'comment_form_default_fields', array(

'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Nama Anda' ) . '</label> ' . ( $req ? '<span>*</span>' : '' ) .

        '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',   

    'email'  => '<p class="comment-form-email">' .

                '<label for="email">' . __( 'Email Anda' ) . '</label> ' .

                ( $req ? '<span>*</span>' : '' ) .

                '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />'.'</p>',

    'url'    => '' ) ),

    'comment_field' => '<p>' .

                '<label for="comment">' . __( 'Komentar Anda:' ) . '</label>' .

                '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>' .

                '</p>',

    'comment_notes_after' => '',

);?>

<?php
comment_form($comment_args); ?>

</div><!-- #comments -->
