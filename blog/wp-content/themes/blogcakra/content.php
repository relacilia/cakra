<article class="post">
    	<div class="row">
        	<div class="col-sm-4">
            	<br>
				<div class="featuredImg"><?php the_post_thumbnail(); ?></div>
            </div>
                
            <div class="col-sm-8">
    
                <h2><?php
                if ( is_single() ) :
                    the_title( '<h1 class="entry-title">', '</h1>' );
                else :
                    the_title( sprintf( '<h2 class="entry-title"><a id="judul" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                endif;
                ?></h2>
        
                <div class="post-info"><a class="entry-time"> <?php  the_time('j F Y'); ?></a> <a class="entry-author">  <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a> </a> <a class="entry-categories">
                
                <?php
                $categories = get_the_category();
                $separator = ", ";
                $output = '';
                
                if ($categories) {
                    foreach ($categories as $category) {
                        $output .= '<a href="' . get_category_link($category->term_id) . '">' . $category->cat_name . '</a>' . $separator;
                    }
                    echo trim($output, $separator);
                }
                ?>
                </a>
                <a class="entry-comments-link">
                <?php 	
                
                    echo '<span class="comments-link">';
                    comments_popup_link( __( '0 Komentar' ), __( '1 Komentar' ), __( '% Komentar' ) );
                    echo '</span>';
                
                ?>
                </a>
                
                  
                </div>
                <script>
                jQuery(document).ready(function(){
                    jQuery('iframe').wrap("<div class='iframe-flexible-container'></div>");
                });
                </script>		
                </p>
                
                 <?php echo wp_trim_words( get_the_content(), 20, '' );?>
                <br />
                <br />
                
                <div class="row">
		        	<div class="col-sm-8">
<div class="view-full-post"><a href="<?php the_permalink() ?>" class="view-full-post-btn">See More</a></div>
        			<br />
                    </div>
                       
   		        	<div class="col-sm-4"> 
                <!-- share buttons-->
                <div id="share-buttonsMain">

                    <!-- Facebook -->
                    <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>" alt="Share on Facebook" title="Share on Facebook" target="_blank">
                    <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
                    </a>
                        
                    <!-- Twitter -->
                    <a href="http://twitter.com/share?text=<?php the_title(); ?> -&url=<?php the_permalink() ?>&via=cakraApp" alt="Tweet This Post" title="Tweet This Post" target="_blank">
                    <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
                    </a>
                    
                    <!-- Google+ -->
                    <a href="https://plusone.google.com/_/+1/confirm?hl=en-US&url=<?php the_permalink() ?>" alt="Share on Google+" title="Share on Google+" target="_blank">
                    <img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />
                    </a> 
                    
                    <!-- Pinterest -->
                    <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" title="Pin This Post">
                    <img src="https://simplesharebuttons.com/images/somacro/pinterest.png" alt="Pinterest" />
                    </a>                    
                </div>
                </div>
                </div>
    
   			</div>
    	</div><br>    	
	</article>  