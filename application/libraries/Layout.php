<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Layout {
    function show($view, $data = array())
    {
        // Get current CI Instance
        $CI = & get_instance();
 
        // Load template views
        $CI->load->view('template/header', $data);
        $CI->load->view($view, $data);
        $CI->load->view('template/footer', $data);
    }

    function showTem($view, $data = array())
    {
        $CI = & get_instance();
        $CI->load->view('template/header', $data);
        $CI->load->view('template/menu', $data);
        $CI->load->view($view, $data);
        $CI->load->view('template/footer', $data);
    }
 
}
 
/* End of file Template.php */