<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Admin_model extends CI_Model {


	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}

	
	
	public function create_user($nama, $username, $password) {
		
		$data = array(
			'nama_lengkap'   		=> $nama,
			'username'      	=> $username,
			'password'   	=> password_hash($password, PASSWORD_DEFAULT)
		);

		$this->db->where('username', $username);
		$query = $this->db->get('administrator');
		
		if($query->num_rows() > 0)
			return false;
		else
			return $this->db->insert('administrator', $data);
		
	}

	public function create_admin ($nama, $username, $password) {
		
		$data = array(
			'nama_lengkap'   		=> $nama,
			'username'      	=> $username,
			'password'   	=> password_hash($password, PASSWORD_DEFAULT)
		);

		return $this->db->insert('administrator', $data);
		
	}

	public function update_user($email, $data){

		$this->db->where('email', $email);

		return $this->db->update('webuser', $data);
		
	}

	public function get_all_admin() {
		
		$this->db->from('administrator');
		return $this->db->get()->result();
		
	}
	
	public function resolve_user_login($username, $password) {
		
		$this->db->select('password');
		$this->db->from('administrator');
		$this->db->where('username', $username);
		$hash = $this->db->get()->row('password');

		return password_verify($password, $hash);
	}
	
	public function get_user($email) {
		
		$this->db->from('administrator');
		$this->db->where('username', $email);
		return $this->db->get()->row();
		
	}

	public function verify_download($email, $kode) {

		$this->db->where('email', $email);
		$query = $this->db->get('webuser')->row();

		$true_kode = MD5($email . 'cakra01');

		if($kode == $true_kode)
			return true;
		else
			return false;
	}


	

}
