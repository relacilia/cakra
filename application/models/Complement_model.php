<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Complement_model extends CI_Model {

	public function __construct() {

		parent::__construct();
		$this->load->database();

	}

  public function get_contact() {
		return $this->db->get('contact')->row();
	}

	public function update($data)
	{
		$this->db->where('ID', 1);

		return $this->db->update('contact', $data);
	}

}
