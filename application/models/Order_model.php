<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Order_model extends CI_Model {


	var $tabel_mahasiswa='tbl_mahasiswa';
	var $tabel_provinsi='provinsi';
	var $tabel_kabupaten='kabupaten';
	var $tabel_kecamatan='kecamatan';
	var $tabel_kelurahan='kelurahan';

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		$this->load->database();

	}

	/**
	 * create_user function.
	 *
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */

	public function get_all_pesanan_user($id_user) {

		$this->db->where('webuser_id', $id_user);
		return $this->db->get('order')->result();
	}

	public function get_pesanan_konfirmasi($id_user) {
		$this->db->where('webuser_id', $id_user);
		$this->db->where('status', 'pending');
		return $this->db->get('order')->result();
	}

	public function get_pesanan_verifikasi() {
		$this->db->where('status', 'proses');
		return $this->db->get('order')->result();
	}


	public function get_detail ($id) {
		$this->db->from('order');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function get_pesanan($id) {
		$this->db->from('order');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function get_order()
	{
		return $this->db->get('order')->result();
	}

	public function delete_order($id) {
		return $this->db->delete('order', array('id' => $id));
	}

	public function record_count($username) {
		$query = $this->db->where('webuser_id', $username)->get('order');
		return $query->num_rows();
	}
	public function record_count_all() {
		$query = $this->db->get('order');
		return $query->num_rows();
	}

	public function fetch_order($limit, $start, $username) {
		$this->db->limit($limit, $start);

		$query = $this->db->where('webuser_id', $username)->get('order');

		if ($query->num_rows() > 0 ) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}

			return $data;
		}

		return false;
	}

	public function fetch_order_all($limit, $start) {
		$this->db->limit($limit, $start);
		$query = $this->db->get('order');

		if ($query->num_rows() > 0 ) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}

			return $data;
		}

		return false;
	}

	public function ambil_provinsi() {
	$sql_prov=$this->db->get($this->tabel_provinsi);
	if($sql_prov->num_rows()>0){
		foreach ($sql_prov->result_array() as $row)
			{
				$result['-']= '- Pilih Provinsi -';
				$result[$row['id_provinsi']]= ucwords(strtolower($row['nama_provinsi']));
			}
			return $result;
		}
	}

	public function ambil_kabupaten($kode_prop){
	$this->db->where('id_provinsi',$kode_prop);
	$this->db->order_by('nama_kabupaten','asc');
	$sql_kabupaten=$this->db->get($this->tabel_kabupaten);
	if($sql_kabupaten->num_rows()>0){

		foreach ($sql_kabupaten->result_array() as $row)
        {
            $result[$row['id_kabupaten']]= ucwords(strtolower($row['nama_kabupaten']));
        }
		} else {
		   $result['-']= '- Belum Ada Kabupaten -';
		}
        return $result;
	}

	public function ambil_kecamatan($kode_kab){
	$this->db->where('id_kabupaten',$kode_kab);
	$this->db->order_by('nama_kecamatan','asc');
	$sql_kecamatan=$this->db->get($this->tabel_kecamatan);
	if($sql_kecamatan->num_rows()>0){

		foreach ($sql_kecamatan->result_array() as $row)
        {
            $result[$row['id_kecamatan']]= ucwords(strtolower($row['nama_kecamatan']));
        }
		} else {
		   $result['-']= '- Belum Ada Kecamatan -';
		}
        return $result;
	}

	public function ambil_kelurahan($kode_kec){
	$this->db->where('id_kecamatan',$kode_kec);
	$this->db->order_by('nama_kelurahan','asc');
	$sql_kelurahan=$this->db->get($this->tabel_kelurahan);
	if($sql_kelurahan->num_rows()>0){

		foreach ($sql_kelurahan->result_array() as $row)
        {
            $result[$row['id_kelurahan']]= ucwords(strtolower($row['nama_kelurahan']));
        }
		} else {
		   $result['-']= '- Belum Ada Kelurahan -';
		}
        return $result;
	}




	public function nama_prov($id)
	{
		$this->db->from('provinsi');
		$this->db->where('id_provinsi', $id);
		return $this->db->get()->row('nama_provinsi');
	}

	public function nama_kab($id)
	{
		$this->db->from('kabupaten');
		$this->db->where('id_kabupaten', $id);
		return $this->db->get()->row('nama_kabupaten');
	}

	public function nama_kec($id)
	{
		$this->db->from('kecamatan');
		$this->db->where('id_kecamatan', $id);
		return $this->db->get()->row('nama_kecamatan');
	}


	public function simpan($data){
		$this->db->insert('order',$data);
	}

	public function get_harga($id)
	{
		$this->db->from('produk');
		$this->db->where('nama_produk', $id);
		return $this->db->get()->row('harga');
	}

	public function konfirm_q($id,$data)
	{
		$this->db->where('id', $id);
        $this->db->update('order', $data);
	}

	public function edit_status($id,$data)
	{
		$this->db->where('id', $id);
        $this->db->update('order', $data);
	}

	function lihat($sampai,$dari){
		$query = $this->db->order_by('ID','DESC')->get('order',$sampai,$dari);
		return $query->result();

	}

	function jumlah(){
		return $this->db->get('order')->num_rows();
	}

	function lihatsearch($find){
		$this->db->where("id LIKE '%$find%' OR Nama_Pemesan LIKE '%$find%'");
		return $query = $this->db->get('order')->result();
	}
}
