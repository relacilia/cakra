<?php if (isset($no_footer) && $no_footer) {} else { ?>
<section id="footer">
    <div class="row" style="margin: 0">
        <div class="col s12 m8 l8 right">
            <div class="pernyataan">
                <p>Copyright 2016. All rights reserved.</p>
            </div>
        </div>
        <div class="col m4 l4 left hide-on-small-only">
            <div class="pernyataan">
                <a><i class="material-icons left">settings_phone</i>+62 813 9065 8281</a>
            </div>
        </div>
    </div>
</section>

<?php } ?>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
    <script>
        $('#form_regis').validate();

        $(document).ready(function(){
            $('.slider').slider({
                indicators: false,
                height: 200,
                interval: 10000
            });
            $('.carousel.a').carousel({
                full_width: true
            });

        });
        $(".button-collapse").sideNav({
            menuWidth: 300,
            edge: 'right'
        });
        $('.collapsible').collapsible();

        $('.dropdown-button').dropdown({
            constrain_width: false,
            hover: true, // Activate on hover
            belowOrigin: true
            }
        );



        function slide_prev() {
            $('.slider').slider('prev');
        }

        function slide_next() {
            $('.slider').slider('next');
        }
    </script>

    <script>
        $(function() {
          $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top - 68
                }, 500);
                return false;
              }
            }
          });
        });
    </script>
    <script>
    $(document).ready(function(){
        $('.materialboxed').materialbox();
      });
    </script>
</body>
</html>
