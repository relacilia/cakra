<div class="navbar-fixed" style="z-index: 996">
    <nav id="menu-top">
        <div class="nav-wrapper">
            <ul>
                <li class="menu-top-logo">
                    <a href="<?php echo base_url(); ?>">
                        <img class="disable_select" src="<?php echo base_url(); ?>assets/img/logo.svg">
                    </a>
                </li>
                <div class="right">

                    <!-- main menu -->
                    <li class="hide-on-med-and-down">
                        <a href="<?php echo site_url('home'); ?>">
                            Home 
                        </a>
                    </li>
                    <li class="hide-on-med-and-down">
                        <a class="dropdown-button" href="#" data-activates='drop-tentang'>
                            Tentang Cakra
                            <i class="material-icons inline-arrow-down deg">keyboard_arrow_up</i>
                        </a>
                        <ul id='drop-tentang' class='dropdown-content bayangan_2dp'>
                            <li><a href="<?php echo site_url('home/autisme'); ?>">Apa itu Autisme?</a></li>
                            <li><a href="<?php echo site_url('home/cakra'); ?>">Apa itu Cakra?</a></li>
                            <li><a href="<?php echo site_url('home/penghargaan'); ?>">Penghargaan Cakra</a></li>
                            <!-- <li><a href="#!">Testimonial</a></li> -->
                        </ul>
                    </li>
                    <!-- <li class="hide-on-med-and-down">
                        <a href="<?php echo site_url('home/produk'); ?>">
                            Produk 
                        </a>
                    </li> -->
                    <!-- <li class="hide-on-med-and-down">
                        <a>
                            FAQ
                        </a>
                    </li> -->
                    <li class="hide-on-med-and-down">
                        <a class="dropdown-button" href="#" data-activates='drop-order'>
                            Order
                            <i class="material-icons inline-arrow-down deg">keyboard_arrow_up</i>
                        </a>
                        <ul id='drop-order' class='dropdown-content bayangan_2dp'>
                            <li><a href="<?php echo site_url('home/cara_order'); ?>">Cara Order</a></li>
                            <!-- <li><a href="#!">Order Cakra</a></li> -->
                            <!-- <li><a href="#!">Konfirmasi Pembayaran</a></li> -->
                        </ul>
                    </li>

                    <!-- action menu -->
                    <?php if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) { ?>
                    <li class="hide-on-small-only actions daftar">
                        <a href="<?php echo site_url('user'); ?>">
                            MY DASHBOARD
                        </a>
                    </li>
                    <?php } else { ?>
                    <li class="hide-on-small-only actions daftar">
                        <a href="<?php echo site_url('user/daftar'); ?>">
                            DAFTAR
                        </a>
                    </li>
                    <li class="hide-on-small-only actions login ">
                        <a href="<?php echo site_url('user'); ?>">
                            LOGIN
                        </a>
                    </li>
                    <?php } ?>


                    <li class="hide-on-large-only menu-top-icon">
                        <a href="#" data-activates="mobile-demo" class="button-collapse">
                            <i class="material-icons left">menu</i>
                        </a>
                    </li>
                </div>
            </ul>
            
        </div>
    </nav>
</div>

<ul class="side-nav" id="mobile-demo">
    <li>
        <a href="<?php echo base_url(); ?>">
            <img class="disable_select" src="<?php echo base_url(); ?>assets/img/logo.svg">
        </a>
    </li>
    <li>
        <a href="<?php echo site_url('home'); ?>">
            <!-- <i class="material-icons">home</i> -->
            <span>Home</span>
        </a>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">
                    <i class="material-icons right">expand_more</i>
                    <span>Tentang Cakra</span>
                </a>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <a href="<?php echo site_url('home/autisme'); ?>">
                                Apa itu Autisme?
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('home/cakra'); ?>">
                                Apa itu Cakra?
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                Penghargaan Cakra
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                Testimonial
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <li>
        <a href="#!">
            <!-- <i class="material-icons">home</i> -->
            <span>Produk</span>
        </a>
    </li>
    <li>
        <a href="#!">
            <!-- <i class="material-icons">home</i> -->
            <span>FAQ</span>
        </a>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">
                    <i class="material-icons right">expand_more</i>
                    <span>Order</span>
                </a>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <a href="#!">
                                Cara Order
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                Order Cakra
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                Konfirmasi Pembayaran
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
</ul>