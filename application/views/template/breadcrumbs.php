<section>
	<div class="dashboard-content">
		<div class="bg-cakra">
			<div class="path">
				<i class="material-icons">home</i>
				<a href="<?php echo site_url('Dashboard'); ?>"><span style="vertical-align: middle; font-size: 15px;"><?php echo $nama_page ?></span></a>

				<?php for($i = 0; $i < count($path_); $i++) { ?>
				<i class="material-icons">chevron_right</i>
				<?php echo $url[$i]; ?><span style="vertical-align: middle; font-size: 15px;"><?php echo $path_[$i]; ?></span>
				<?php } ?>
			</div>
		</div>
