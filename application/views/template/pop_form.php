<div id="dua" class="modal modal-fixed-footer">
	<div class="modal-content">
		<section id="daftar" style="min-height: 100vh; background-color: rgba(8,64,61, .5)">
	<div class="container bayangan_2dp" style="background-color: white; width: 80%; padding: 15px 30px;">
        <div class="row">

            <div class="col s12">
                <div class="bg-cakra ">

					<div style="text-align: center; margin-bottom: 20px">
						<img style="height: 100px; padding: 15px 0;" class="disable_select" src="<?php echo base_url(); ?>assets/img/logo.svg">
						<p style="font-size: 2rem; color: rgba(8,64,61, .7)">Formulir Pendaftaran Admin</p>
	                </div>
					<div class="isi-content form">
						<form id="form_regis" method="POST" action="<?php echo site_url('dashboard_admin/submit_admin'); ?>">
							<div class="row">
								<?php if($this->session->flashdata('pesan')) { ?>
							    <div class="col s12" style="padding: 0 1.75rem; margin-bottom: 15px;">
							        <div class="chip <?php echo $this->session->flashdata('tipe'); ?>">
							            <i class="material-icons left">error</i>
							            <i class="material-icons right">close</i>
							            <p><?php echo $this->session->flashdata('pesan'); ?></p>
							            
							        </div>
							    </div>
							    <?php } ?>
					        	<div class="col s12">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Nama Lengkap <span style="color: red">*</span></span>
					                            <input id="nama" name="nama" placeholder="Nama Lengkap" type="text" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
					        	<div class="col s12">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Username <span style="color: red">*</span></span>
					                            <input id="uname" name="uname" placeholder="Username" type="text" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
					        	<div class="col s12">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Password <span style="color: red">*</span></span>
					                            <input id="password" name="password" placeholder="Password" type="password" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
					        	

				        	<div class="action center">
				        		<input class="btn-flat" type="submit">
					
				        	</div>
					    </form>

					    
					</div>
				</div>
				<div class="col s6">
            </div>
        </div>
    </div>
</section>

	</div>
	<div class="modal-footer">
		<div class="action no-padding">
            <a href="" class="btn-flat" data-warna="grey">
            	close
            </a>
    	</div>
	</div>
</div>