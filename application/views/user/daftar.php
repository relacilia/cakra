<section id="daftar" style="min-height: 100vh; background-color: rgba(8,64,61, .5)">
	<div class="container bayangan_2dp" style="background-color: white; width: 80%; padding: 15px 30px;">
        <div class="row">

            <div class="col s12">
                <div class="bg-cakra ">

					<div style="text-align: center; margin-bottom: 20px">
						<img style="height: 100px; padding: 15px 0;" class="disable_select" src="<?php echo base_url(); ?>assets/img/logo.svg">
						<p style="font-size: 2rem; color: rgba(8,64,61, .7)">Formulir Pendaftaran Anggota</p>
	                </div>
					<div class="isi-content form">
						<form id="form_regis" method="POST" action="<?php echo site_url('user/submit_user'); ?>">
							<div class="row">
								<?php if($this->session->flashdata('pesan')) { ?>
							    <div class="col s12" style="padding: 0 1.75rem; margin-bottom: 15px;">
							        <div class="chip <?php echo $this->session->flashdata('tipe'); ?>">
							            <i class="material-icons left">error</i>
							            <i class="material-icons right">close</i>
							            <p><?php echo $this->session->flashdata('pesan'); ?></p>
							            
							        </div>
							    </div>
							    <?php } ?>
					        	<div class="col s6">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Email <span style="color: red">*</span></span>
					                            <input id="email" name="email" placeholder="Email" type="email" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
					        	<div class="col s6">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Password <span style="color: red">*</span></span>
					                            <input id="password" name="password" placeholder="Password" type="password" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
					        	<div class="col s4">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Nama Lengkap <span style="color: red">*</span></span>
					                            <input id="nama" name="nama" placeholder="Nama Lengkap" type="text" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
					        	<div class="col s4">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Nomer HP/Telepon <span style="color: red">*</span></span>
					                            <input id="nohp" name="nohp" placeholder="Nomer HP/Telepon" type="number" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
					        	<div class="col s4">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Profesi</span>
					                            <input id="profesi" name="profesi" placeholder="Profesi" type="text" class="validate">
					                        </div>
						        		</div>
						        	</div>
					        	</div>
					        	
					        	<div class="col s12">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field textarea">
					                            <span>Alamat <span style="color: red">*</span></span>
					                            <textarea id="alamat" name="alamat" required></textarea>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
				        	</div>

				        	<div class="action center">
				        		<input class="btn-flat" type="submit">
					            <!-- <a href="javascript:{}" onclick="document.getElementById('form_regis').submit();" class="btn-flat" data-warna="cakra">
					            	<i class="material-icons left">assignment_turned_in</i>SUBMIT
					            </a> -->
					            <!-- <a href="<?php echo site_url('user') ?>" class="btn-flat" data-warna="grey">
					            	<i class="material-icons left">close</i>BATAL
					            </a>  -->
				        	</div>
					    </form>

					    
					</div>
				</div>
				<div class="col s6">
                            <div class="daftar" style="padding-top: 30px;">
                                <p>
                                	<a href="<?php echo site_url('/');?>">
                                		<i class="material-icons left" style="font-size: 100%; margin: 0">keyboard_arrow_left</i>
                                		<i class="material-icons left" style="font-size: 100%;">keyboard_arrow_left</i>
                                		<strong>Kembali ke halaman utama</strong>
                                	</a>
	                            </p>
                            </div>
                        </div>
					    
                        <div class="col s6">
                            <div class="daftar right" style="padding-top: 30px;">
                                <p>
                                	<a href="<?php echo site_url('user');?>">
                                		<i class="material-icons right" style="font-size: 100%; margin: 0">keyboard_arrow_right</i>
                                		<i class="material-icons right" style="font-size: 100%;">keyboard_arrow_right</i>
                                		<strong>Halaman Login</strong>
                                	</a>
	                            </p>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</section>