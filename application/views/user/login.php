<section id="login">
    <div class="container opening" style="padding: 0; padding-top: 25vh">
        <div class="login-form">
            <div class="form-get-cakra">
                <p>User Login</p>
            </div>
            <div class="row">
                <?php if($this->session->flashdata('pesan')) { ?>
                <div class="col s12" style="padding: 0 1.75rem; margin-bottom: 15px;">
                    <div class="chip <?php echo $this->session->flashdata('tipe'); ?>">
                        <i class="material-icons left">error</i>
                        <i class="material-icons right">close</i>
                        <p><?php echo $this->session->flashdata('pesan'); ?></p>
                        
                    </div>
                </div>
                <?php } ?>
                <form class="col s12" id="form_login" method="post" action="<?php echo site_url('user/login')?>">
                    <div class="row">
                        <div class="input-field col s12">
                            <!-- <span>Nama</span> -->
                            <input id="email" name="email" placeholder="Email" type="email" class="validate">
                        </div>
                        <div class="input-field col s12">
                            <!-- <span>Nama</span> -->
                            <input id="password" name="password" placeholder="Password" type="password" class="validate">
                        </div>
                        <div class="col s6">
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box" />
                                <label for="filled-in-box">Ingatkan saya</label>
                            </p>
                        </div>
                        <div class="col s6" style="padding: 0 1.75rem; text-align: right">
                            <!-- <p style="float: right"> -->
                                <!-- <input type="checkbox" class="filled-in" id="filled-in-box" checked="checked" /> -->
                                <a href="#">Lupa Password?</a>
                            <!-- </p> -->
                        </div>
                        <div class="col s12" style="text-align: center">
                            <a href="javascript:{}" onclick="document.getElementById('form_login').submit();" class="btn-flat bayangan_2dp">Login</a>
                        </div>
                        <div class="col s12">
                            <div class="daftar">
                                <p>Belum terdaftar? <a href="<?php echo site_url('user/daftar');?>"><strong>Daftar sekarang</strong></a></p>
                            </div>
                        </div>
                        <div class="col s12">
                            <div class="daftar" > 
                                <p>
                                    <a href="<?php echo site_url('/');?>">
                                        << Kembali ke halaman utama
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>