<div class="navbar-fixed dashboard-menu" style="z-index: 996">
    <nav id="menu-top" >
        <div class="nav-wrapper">
            <ul>
                <li class="menu-top-icon" >
                    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin: 0; margin-right: 10px">
                        <i class="material-icons left">menu</i>
                    </a>
                </li>
                <li class="menu-top-logo">
                    <a href="<?php echo base_url(); ?>">
                        <img class="disable_select" src="<?php echo base_url(); ?>assets/img/logo.svg">
                    </a>
                </li>
                <div class="right">
                    <li>
                        <a class="dropdown-button" href="#" data-activates='drop-tentang' style="font-weight: 600; text-transform: none">
                            <i class="material-icons left">account_circle</i>
                            <span class="hide-on-small-only"><?php echo $_SESSION['username']; ?></span>
                            <i class="material-icons inline-arrow-down deg">keyboard_arrow_up</i>
                        </a>
                        <ul id='drop-tentang' class='dropdown-content bayangan_2dp'>
                            <li>
                                <a href="<?php echo site_url('dashboard/logout'); ?>">
                                    <i class="material-icons left">exit_to_app</i>
                                    <span class="hide-on-small-only">Logout</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </div>

            </ul>

        </div>
    </nav>
</div>

<ul class="side-nav fixed dashboard-nav" id="mobile-demo">
    <li>
        <a href="<?php echo base_url(); ?>">
            <img class="disable_select" src="<?php echo base_url(); ?>assets/img/logo.svg">
        </a>
    </li>
    <li class="<?php if($menu_aktif == 'dash') echo 'active-true'; ?>">
        <a href="<?php echo site_url('dashboard'); ?>">
            <i class="material-icons left">account_balance_wallet</i>
            <span>Dasboard</span>
        </a>
    </li>

     <!-- awal -->
        <li class="<?php if($menu_aktif == 'list_pesan') echo 'active-true'; ?>">
        <a href="<?php echo site_url('dashboard_admin/list_pesan'); ?>">
            <i class="material-icons">note</i>
            <span>List Pemesanan</span>
        </a>

        <li class="<?php if($menu_aktif == 'list_akun') echo 'active-true'; ?>">
        <a href="<?php echo site_url('dashboard_admin/list_akun'); ?>">
            <i class="material-icons">perm_identity</i>
            <span>List Akun</span>
        </a>
        <li class="<?php if($menu_aktif == 'edit_kontak') echo 'active-true'; ?>">
        <a href="<?php echo site_url('dashboard_admin/u_kontak'); ?>">
            <i class="material-icons">edit</i>
            <span>Edit Kontak</span>
        </a>
   <!-- akhir -->

</ul>
