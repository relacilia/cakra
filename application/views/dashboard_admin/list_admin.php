<section>
	<div class="dashboard-content">
		<div class="bg-cakra">
			<div class="path">
				<i class="material-icons">home</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $nama_page ?></span>

				<?php for($i = 0; $i < count($path_); $i++) { ?>
				<i class="material-icons">chevron_right</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $path_[$i]; ?></span>
				<?php } ?>
			</div>
		</div>
		<br>
		<div>
			<a class="waves-effect waves-light btn" href="<?php echo base_url();?>dashboard_admin/list_akun">Akun User</a>
			<a class="waves-effect waves-light btn" style="background:#8c9eff;" href="<?php echo base_url();?>dashboard_admin/list_admin">Akun admin</a>
    </div>
    	<br>
    	<?php if($this->session->flashdata('pesan')) { ?>
							    <div class="col s12" style="padding: 0 1.75rem; margin-bottom: 15px;">
							        <div class="chip <?php echo $this->session->flashdata('tipe'); ?>">
							            <i class="material-icons left">error</i>
							            <i class="material-icons right">close</i>
							            <p><?php echo $this->session->flashdata('pesan'); ?></p>

							        </div>
							    </div>
							    <?php } ?>

		<div class="content">
			<div class="row">

				<div class="col s12" >
					<?php $this->load->view('dashboard_admin/dasha_admin'); ?>
				</div>
			</div>
		</div>

	</div>
</section>
