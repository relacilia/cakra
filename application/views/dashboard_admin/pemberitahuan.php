<div class="bg-cakra">
    <div class="nama-content">
        <i class="material-icons left">watch_later</i>
        <span>Reminder</span>
    </div>
    <div class="isi-content">
      <div class="field success" style="text:bold">
                    <i>
                        <i class="material-icons left" icon="notifications_active"></i>
                        Silahkan segera mengirim bukti transaksi
                    </i>
        </div>
        <div class="riwayat">
            <table class="striped">
                <thead>
                    <tr>
                        <th data-field="id">ID</th>
                        <th data-field="name">Produk</th>
                        <th data-field="tanggal">Tanggal Pesan</th>
                        <th data-field="total_harga">Total Harga</th>
                        <th class="center" data-field="action">Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($riwayat) == 0) { ?>

                    <?php } ?>
                    <?php for($i=0; $i<count($riwayat); $i++) { ?>
                        <tr>
                            <td>#<?php echo $riwayat[$i]->id; ?></td>
                            <td style="text-transform: capitalize">cakra <?php echo $riwayat[$i]->edition; ?></td>
                            <td><?php echo $riwayat[$i]->created; ?></td>
                            <td><?php echo $riwayat[$i]->Biaya_Total; ?></td>

                            <td class="center">

                                <div class="action">
                                    <a href="<?php echo base_url(); ?>/dashboard_admin/edit_status/<?php echo $riwayat[$i]->id; ?>" class="btn-flat" data-warna="deep-blue" >
                                        <i class="material-icons left">done_all</i>
                                        Verifikasi
                                    </a>
                                    <a href="<?php echo base_url(); ?>/dashboard/riwayat" class="btn-flat" data-warna="green">
                                        <i class="material-icons left">search</i>
                                        Selengkapnya
                                    </a>
                                </div>
                            </td>

                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('template/pop_persetujuan'); ?>
