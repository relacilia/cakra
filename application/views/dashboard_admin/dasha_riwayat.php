<div class="bg-cakra">
    <div class="nama-content">
      <form method="post" action="<?php echo base_url()?>Dashboard_admin/search">
        <div class="row" style="margin-bottom:0px;">
        <div class="col s6" >
        <i class="material-icons left">history</i>
        <span>List Pemesanan</span>
        </div>
        <div class="col s4" >
            <div class="box">
              <div class="container-4">
                <input type="search" id="search" name="find" placeholder="Search..." />
                <button type="submit" class="icon" style="height:38px;"><i class="fa fa-search"></i></button>
              </div>
            </div>
        </div>
        <div class="col s2" >
            <a href="<?php echo base_url(); ?>dashboard_admin/pesan_paket" class="waves-effect waves-light btn modal-trigger" style="background:#8c9eff;font-size:11px">Tambah Pesanan</a>
        </div>
        </div>

    </div>
    <div class="isi-content">
        <div class="riwayat">
            <table class="striped">
                <thead>
                    <tr>
                        <th data-field="id">ID</th>
                        <th data-field="name">Produk</th>
                        <th data-field="tanggal">Tanggal Pesan</th>
                        <th data-field="status">Status</th>
                        <th class="center" data-field="action">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                         //kalo data tidak ada didatabase
                         if(empty($query))
                         {
                             echo "<tr><td colspan=\"6\">Data tidak tersedia</td></tr>";
                         }else
                         {
                             $no = 1;
                             foreach($query as $riwayat)
                             {
                         ?>
                        <tr>
                            <td>#<?php echo $riwayat->id; ?></td>
                            <td style="text-transform: capitalize">cakra <?php echo $riwayat->edition; ?></td>
                            <td><?php echo $riwayat->created; ?></td>
                            <td>
                                <div class="action">
                                    <a class="btn-flat" data-warna="<?php echo $riwayat->status; ?>">
                                        <?php echo $riwayat->status; ?>
                                    </a>
                                </div>
                            </td>

                            <td class="center">

                                <div class="action">
                                    <a href="<?php echo base_url(); ?>/dashboard_admin/lihat_detail/<?php echo $riwayat->id; ?>" class="btn-flat" data-warna="deep-blue">
                                        <i class="material-icons left">mode_edit</i>
                                        Lihat Detail / Edit
                                    </a>

                                    <a href="<?php echo base_url(); ?>/dashboard_admin/hapus/<?php echo $riwayat->id; ?>" class="btn-flat" data-warna="red">
                                        <i class="material-icons left">close</i>
                                        batal
                                    </a>
                                </div>

                            </td>

                        </tr>
                        <?php
                          $no++;
                         }}
                     ?>

                </tbody>
            </table>
            <div class="halaman" style="margin-top:15px;">Halaman : <?php echo $halaman;?></div>
            </form>
        </div>
    </div>
</div>

<?php $this->load->view('template/pop_persetujuan'); ?>
