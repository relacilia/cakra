<div class="bg-cakra">
    <div class="nama-content">
        <div class="row" style="margin-bottom:0px;">
        <div class="col s10" >
        <i class="material-icons left">perm_identity</i>
        <span>List Akun User</span>
        </div>
        <div class="col s2" >
            <a href="#dua" class="waves-effect waves-light btn modal-trigger" style="background:#8c9eff;font-size:12px">Tambah Admin</a>
        </div>
        </div>
    </div>
    <div class="isi-content">
        <div class="riwayat">
            <table class="striped">
                <thead>
                    <tr>
                        <th data-field="id">ID</th>
                        <th data-field="name">Nama</th>
                        <th data-field="tanggal">Username</th>
                        <th class="center" data-field="action">Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($akun) == 0) { ?>

                    <?php } ?>
                    <?php for($i=0; $i<count($akun); $i++) { ?>
                        <tr>
                            <td>#<?php echo $akun[$i]->id; ?></td>
                            <td><?php echo $akun[$i]->nama_lengkap; ?></td>
                            <td><?php echo $akun[$i]->username; ?></td>
                            <td>
                                <div class="action" style="text-align:center">
                                    <a href="<?php echo base_url(); ?>/dashboard_admin/hapus_akun/<?php echo $akun[$i]->id; ?>" class="btn-flat" data-warna="red">
                                        <i class="material-icons left">close</i>
                                        Hapus
                                    </a>
                                </div>
                            </td>


                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('template/pop_form'); ?>
