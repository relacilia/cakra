<section>
	<div class="dashboard-content">
		<!-- <h4 class="nama-dash">
			Selamat datang, <strong><?php echo $_SESSION['nama']; ?>!</strong>
		</h4> -->
		<div class="bg-cakra">
			<div class="path">
				<i class="material-icons">home</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $nama_page ?></span>

				<?php for($i = 0; $i < count($path_); $i++) { ?>
				<i class="material-icons">chevron_right</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $path_[$i]; ?></span>
				<?php } ?>
			</div>
		</div>

		<div class="content">
			<div class="row">
				
				<div class="col s8">
					<div class="bg-cakra ">

					<div style="text-align: center; margin: 10px">
						<h5 style="font-size: 2rem;
						    font-weight: bold;
						    color: rgba(8,64,61, .7);
						    margin: 10px;
						    text-align: center;">Isi Data Pemesanan</h5>
	                </div>
					<div class="isi-content form">
						<form id="form_regis" method="POST" action="<?php echo site_url('Dashboard_admin/add_order/'.$tipe); ?>">
							<div class="row">
								<!--<?php if($this->session->flashdata('pesan')) { ?>
							    <div class="col s12" style="padding: 0 1.75rem; margin-bottom: 15px;">
							        <div class="chip <?php echo $this->session->flashdata('tipe'); ?>">
							            <i class="material-icons left">error</i>
							            <i class="material-icons right">close</i>
							            <p><?php echo $this->session->flashdata('pesan'); ?></p>
							            
							        </div>
							    </div>
							    <?php } ?> -->

							    	<div class="field">
							    		<div class="isi">
						                        <span>Pesan Paket :</span>
					                            <span style="font-weight:bolder;color:#14a089;font-size:larger"><?php echo $tipe ?></span>
					                        </div>
					                </div>
					        	
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Nama Lengkap Pemesan <span style="color: red">*</span></span>
					                            <input id="" name="name" placeholder="Nama" type="" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	
					        	
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Email Pemesan<span style="color: red">*</span></span>
					                            <input id="" name="email" placeholder="Email" type="" class="validate" required>
					                        </div>
						        		</div>
						        	</div>

						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Nomer HP/Telepon Pemesan<span style="color: red">*</span></span>
					                            <input id="" name="nomor" placeholder="Email" type="" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	
					        	<div class="col s4">
					        		<div class="form-group">
					        			    <div class="field">
					        			    <div class="isi">
                                            <span style="margin-bottom:10px;">Pilih Provinsi<span style="color: red">*</span></span>
                                            
												<?php
													$style_provinsi='class="form-control input-sm" id="provinsi_id"  onChange="tampilKabupaten()"';
													echo form_dropdown('provinsi_id',$provinsi,'',$style_provinsi);
												?>
											</div>
										</div>
						        	</div>
					        	</div>
					        	<div class="col s4">
					        			<div class="field">
					        			    <div class="isi">
                                            <span style="margin-bottom:10px;">Pilih Kota/Kabupaten<span style="color: red">*</span></span>
                                                <?php
												$style_kabupaten='class="form-control input-sm" id="kabupaten_id" onChange="tampilKecamatan()"';
												echo form_dropdown("kabupaten_id",array('Pilih Kabupaten'=>'- Pilih Kabupaten -'),'',$style_kabupaten);
												?>
											</div>
										</div>
					        	</div>
					        	<div class="col s4">
						        	<div class="field">
					        			    <div class="isi">
                                            <span style="margin-bottom:10px;">Pilih Kecamatan<span style="color: red">*</span></span>
								                 <?php
												$style_kecamatan='class="form-control input-sm" id="kecamatan_id"';
												echo form_dropdown("kecamatan_id",array('Pilih Kecamatan'=>'- Pilih Kecamatan -'),'',$style_kecamatan);
												?>
											</div>
										</div>
					        	</div>
					        	
					        	<div class="col s12">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field textarea">
					                            <span>Alamat Lengkap<span style="color: red">*</span></span>
					                            <textarea id="alamat" name="alamat" placeholder="Kelurahan, RT, RW, Jalan, dll" required></textarea>
					                        </div>
						        		</div>
						        	</div>
					        	</div>

					        	<div class="col s4">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Kode Pos<span style="color: red">*</span></span>
					                            <input id="" name="kode" placeholder="Kode Pos" type="" class="validate" required>
					                        </div>
						        		</div>
						        	</div>
					        	</div>

					        	<div class="col s8">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Jasa Pengiriman</span>
					                            <input id="" name="jasa" placeholder="Untuk saat ini belum bisa memilih" type="" disabled>
					                        </div>
						        		</div>
						        	</div>
					        	</div>
				        	</div>

				        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span style="margin-right:5%">Jumlah Barang</span>
				        	
								<!-- <input type="button" onclick="decrementValue()" value="-" /> -->
								<input type="text" name="jumlah" value="1" maxlength="2" max="50" size="1" id="number" class= "a2" onkeyup="hitung2();" style="width:10%;"/>
								<!-- <input type="button" onclick="incrementValue()" value="+" /> -->
						
						</div>
					</div>
				</div>

				        	<div class="action center">
				        		<input class="btn-flat" type="submit">
					            <!-- <a href="javascript:{}" onclick="document.getElementById('form_regis').submit();" class="btn-flat" data-warna="cakra">
					            	<i class="material-icons left">assignment_turned_in</i>SUBMIT
					            </a> -->
					            <!-- <a href="<?php echo site_url('user') ?>" class="btn-flat" data-warna="grey">
					            	<i class="material-icons left">close</i>BATAL
					            </a>  -->
				        	</div>
					    </form>

					    
					</div>
					<!-- <p style="font-size: 1.3rem; font-weight:bold; color: rgba(8,64,61, .7); margin: 10px;">Detail Barang yang dipesan </p>
					<div class="isi-content form">
						<p>Paket yang dipesan : Silver </p>

				</div> -->
				</div>


			</div>

				 <div class="col s4">
					<div class="bg-cakra " style="position:fixed;">
					<div style="text-align: left; margin: 10px">
						<p style="font-size: 1.3rem; font-weight:bold; color: rgba(8,64,61, .7); margin: 10px;">Ringkasan Biaya Pemesanan </p>
						<div class="ringkas">
							<p>Paket yang dipesan : <strong> <?php echo $tipe ?> </strong> </p>
							<p>Harga Barang: Rp <?php echo $harga ?>,00 </p>
							<p id='jum'>Jumlah Barang : 

								<script>
									$( "input" )
									  .change(function() {
									    var value = $( '#number' ).val();
									    $( "#jum" ).text('Jumlah Barang: ' + value );
									  })
									  .change();
									</script>




							</p>
							<p>Biaya Kirim : <strong> Gratis </strong> </p>
							<hr>
							<p style="font-size: 1rem; font-weight:bold; color: black;" id="c2">Total Biaya :
								<div class="field">
						        		<div class="isi">
						        			<div class="input-field" style="text-align:center">
								Rp <input class="c2" name="biaya" type="text" style="width: 40%;background: ghostwhite;border: none;margin-top: -20%;font-size: 25px;padding: 0;" readonly />,00

									<script>
									$(window).ready(function(){
										hitung2();
									});

									function hitung2() {
									    var a = $(".a2").val();
									    var b = '<?php echo $harga ?>';
									    c = a * b; //a kali b
									    $(".c2").val(c);
									}
									</script>
									</div>
								</div>
								</div>

							 </p>
						</div>
	                </div>
	            </div>
	        </div> 
			</div>
		</div>
		
	</div>
</section>





<script src="<?php echo base_url();?>assets/js/jquery-2.1.1.js"></script>
<script>
function tampilKabupaten()
 {
	 kdprop = document.getElementById("provinsi_id").value;
	 $.ajax({
		 url:"<?php echo base_url();?>Dashboard/pilih_kabupaten/"+kdprop+"",
		 success: function(response){
		 $("#kabupaten_id").html(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }
 
 function tampilKecamatan()
 {
	 kdkab = document.getElementById("kabupaten_id").value;
	 $.ajax({
		 url:"<?php echo  base_url();?>Dashboard/pilih_kecamatan/"+kdkab+"",
		 success: function(response){
		 $("#kecamatan_id").html(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }

</script>

<script type="text/javascript">
function incrementValue()
{
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    if(value<10){
        value++;
            document.getElementById('number').value = value;
    }
}
function decrementValue()
{
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    if(value>1){
        value--;
            document.getElementById('number').value = value;
    }

}
</script>