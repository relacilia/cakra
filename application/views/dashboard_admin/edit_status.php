<section>
	<div class="dashboard-content">
		<!-- <h4 class="nama-dash">
			Selamat datang, <strong><?php echo $_SESSION['nama']; ?>!</strong>
		</h4> -->
		<div class="bg-cakra">
			<div class="path">
				<i class="material-icons">home</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $nama_page ?></span>

				<?php for($i = 0; $i < count($path_); $i++) { ?>
				<i class="material-icons">chevron_right</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $path_[$i]; ?></span>
				<?php } ?>
			</div>
		</div>

		<div class="content">
			<div class="row">
				<p style="font-size: 1.3rem; font-weight:bold; color: rgba(8,64,61, .7); margin: 10px; ">Detail Pemesanan ID: <?php echo $order->id ?> </p>

				<div class="col s12">
					<div class="row">
							<div class="col s6">
							<div class="row">
								<div class="col s12">
								<div class="bg-cakra ">
								<div style="text-align: left; margin: 10px">
								<div class="ringkas">
								<table>
									<tr><td style="width:32%">User_Id</td><td>:</td><td><?php echo $order->webuser_id ?></td></tr>
									<tr><td style="width:32%">Tanggal Pesan</td><td>:</td><td><?php echo $order->created ?></td></tr>
									<tr><td style="width:32%">Jenis </td><td>:</td><td><?php echo $order->edition?></td></tr>
									<tr><td style="width:32%">Jumlah Barang</td><td>:</td><td><?php echo $order->Jumlah_Barang ?></td></tr>
									<tr><td style="width:32%">Biaya Total</td><td>:</td><td><?php echo $order->Biaya_Total ?></td></tr>
									<tr><td style="width:32%">Photo</td><td>:</td><td>
										<a target="_blank" href="<?php echo base_url()?>uploads/<?php echo $order->nama_photo; ?>"> file_gambar
									</td></tr>
									<tr><td style="width:32%">Status</td><td>:</td><td><?php echo $order->status ?> </td><td><button id="tombol">Edit</button></td></tr>
								</table>
								</div>
								</div>
								</div>
								</div>
								<div class="col s12" id="kotak" style="display:none">
								<div class="bg-cakra ">
								<div style="text-align: left; margin: 10px">
								<div class="ringkas">
									<form id="form_regis" method="POST" action="<?php echo site_url('Dashboard_admin/edit_status/'.$order->id); ?>">
											<div class="form-group">
					        			    <div class="field">
					        			    <div class="isi">
                                            <span style="margin-bottom:10px;">Ubah Status</span>
                                            <br>
                                            	<?php
                                            		$options = array(
												        'pending'         => 'pending',
												        'proses'           => 'proses',
												        'terkirim'         => 'terkirim'
												     );
													$style_status='class="form-control input-sm" id="status"';
													echo form_dropdown('status',$options,'',$style_status);
												?>
											</div>
										</div>
						        	</div>

							        	<div class="action center">
				        		<input class="btn-flat" type="submit">
					            <!-- <a href="javascript:{}" onclick="document.getElementById('form_regis').submit();" class="btn-flat" data-warna="cakra">
					            	<i class="material-icons left">assignment_turned_in</i>SUBMIT
					            </a> -->
					            <!-- <a href="<?php echo site_url('user') ?>" class="btn-flat" data-warna="grey">
					            	<i class="material-icons left">close</i>BATAL
					            </a>  -->
				        	</div>


									</form>
								</div>
								</div>
								</div>
							</div>




								</div>

							</div>

							<div class="col s6">
								<div class="bg-cakra ">
								<div style="text-align: left; margin: 10px">
								<div class="ringkas">
								<table>
									<tr><td style="width:32%">Nama Pemesan</td><td>:</td><td><?php echo $order->Nama_Pemesan ?></td></tr>
									<tr><td style="width:32%">Email Pemesan</td><td>:</td><td><?php echo $order->Email_Pemesan ?></td></tr>
									<tr><td style="width:32%">Nomor HP</td><td>:</td><td><?php echo $order->Nomor_HP ?></td></tr>
									<tr><td style="width:32%">Propinsi</td><td>:</td><td><?php echo $order->Provinsi ?></td></tr>
									<tr><td style="width:32%">Kota/Kabupaten</td><td>:</td><td><?php echo $order->Kabupaten ?></td></tr>
									<tr><td style="width:32%">Kecamatan</td><td>:</td><td><?php echo $order->Kecamatan ?></td></tr>
									<tr><td style="width:32%">Alamat</td><td>:</td><td><?php echo $order->Alamat ?></td></tr>
									<tr><td style="width:32%">Kode_Pos</td><td>:</td><td><?php echo $order->Kode_Pos ?></td></tr>
									<tr><td style="width:32%">Jasa Pengiriman</td><td>:</td><td><?php echo $order->Jasa_Pengiriman ?></td></tr>
								</table>
								</div>
								</div>
								</div>
							</div>
					</div>
				</div>
	</div>
	</div>
</section>

<!--<?php $this->load->view('dashboard/dash_riwayat'); ?> -->
