<div class="bg-cakra">
    <div class="nama-content">
        <i class="material-icons left">perm_identity</i>
        <span>List Akun User</span>
    </div>
    <div class="isi-content">
        <div class="riwayat">
            <table class="striped">
                <thead>
                    <tr>
                        <th data-field="id">Tanggal</th>
                        <th data-field="name">Nama</th>
                        <th data-field="tanggal">Email</th>
                        <th data-field="status">Nomor HP</th>
                        <th data-field="status">Alamat</th>
                        <th data-field="status">Profesi</th>
                        <th class="center" data-field="action">Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                         //kalo data tidak ada didatabase
                         if(empty($query))
                         {
                             echo "<tr><td colspan=\"6\">Data tidak tersedia</td></tr>";
                         }else
                         {
                             $no = 1;
                             foreach($query as $akun)
                             {
                         ?>
                        <tr>
                            <td><?php echo $akun->created; ?></td>
                            <td><?php echo $akun->name; ?></td>
                            <td><?php echo $akun->email; ?></td>
                            <td><?php echo $akun->phone; ?></td>
                            <td><?php echo $akun->address; ?></td>
                            <td><?php echo $akun->profession; ?></td>
                            <td>
                                <div class="action">                                    
                                    <a href="<?php echo base_url(); ?>/dashboard_admin/hapus_akun/<?php echo $akun->id; ?>" class="btn-flat" data-warna="red">
                                        <i class="material-icons left">close</i>
                                        Hapus
                                    </a> 
                                </div>
                            </td>
                            
                            
                        </tr>
                    <?php
                          $no++;
                         }}
                     ?>
                    
                </tbody>
            </table>

             <div class="halaman" style="margin-top:15px;">Halaman : <?php echo $halaman;?></div>
        </div>
    </div>
</div>