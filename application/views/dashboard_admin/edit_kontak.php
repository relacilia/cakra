<section>
	<div class="dashboard-content">
		<!-- <h4 class="nama-dash">
			Selamat datang, <strong><?php echo $_SESSION['nama']; ?>!</strong>
		</h4> -->
		<div class="bg-cakra">
			<div class="path">
				<i class="material-icons">home</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $nama_page ?></span>

				<?php for($i = 0; $i < count($path_); $i++) { ?>
				<i class="material-icons">chevron_right</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $path_[$i]; ?></span>
				<?php } ?>
			</div>
		</div>
		<div class="content">
			<div class="row">

				<div class="col s8">
					<div class="bg-cakra ">

					<div style="text-align: center; margin: 10px">
						<h5 style="font-size: 2rem;
						    font-weight: bold;
						    color: rgba(8,64,61, .7);
						    margin: 10px;
						    text-align: center;">Ubah Contact</h5>
	                </div>
					<div class="isi-content form">
						<form id="form_regis" method="POST" action="<?php echo site_url('Dashboard_admin/ubah_kontak'); ?>">
							<div class="row">
						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>No Contact Person</span>
					                            <input id="" name="no_cp" value="<?php echo $contact->no_cp?>" type="">
					                        </div>
						        		</div>
						        	</div>



						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Nama Contact Person</span>
					                            <input id="" name="nama_cp" value="<?php echo $contact->nama_cp?>" type="">
					                        </div>
						        		</div>
						        	</div>

						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>Nama Bank</span>
					                            <input id="" name="nama_bank" value="<?php echo $contact->nama_bank?>" type="" >
					                        </div>
						        		</div>
						        	</div>

						        	<div class="field">
						        		<div class="isi">
						        			<div class="input-field">
					                            <span>No Rekening</span>
					                            <input id="" name="no_rekening" value="<?php echo $contact->no_rekening?>" type="">
					                        </div>
						        		</div>
						        	</div>

											<div class="field">
												<div class="isi">
													<div class="input-field">
																			<span>Nama Pemilik Rekening</span>
																			<input id="" name="nama_rekening" value="<?php echo $contact->nama_rekening?>" type="">
																	</div>
												</div>
											</div>

											<div class="field">
												<div class="isi">
													<div class="input-field">
																			<span>Email Cakra</span>
																			<input id="" name="email" value="<?php echo $contact->email?>" type="">
																	</div>
												</div>
											</div>
				        	<div class="action center">
				        		<input class="btn-flat" type="submit">
				        	</div>
					    </form>


					</div>
				</div>


			</div>

			</div>
		</div>

	</div>
</section>
