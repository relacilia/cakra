<section>
	<div class="dashboard-content">
		<!-- <h4 class="nama-dash">
			Selamat datang, <strong><?php echo $_SESSION['nama']; ?>!</strong>
		</h4> -->
		<div class="bg-cakra">
			<div class="path">
				<i class="material-icons">home</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $nama_page ?></span>

				<?php for($i = 0; $i < count($path_); $i++) { ?>
				<i class="material-icons">chevron_right</i>
				<span style="vertical-align: middle; font-size: 15px;"><?php echo $path_[$i]; ?></span>
				<?php } ?>
			</div>
		</div>

		<div class="content">
			<div class="row">

				<div class="col s12">
					<?php $this->load->view('dashboard_admin/dasha_riwayat'); ?>
				</div>
			</div>
		</div>

	</div>
</section>
