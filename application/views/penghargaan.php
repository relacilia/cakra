<section id="autisme">
    <div class="container">
        <h3>Pengahargaan Cakra</h3>
        <div class="row">
            <div class="col s12 m4 l4">
                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/imaginecuplogo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>ImagineCup merupakan kompetisi terbesar yang diadakan oleh Microsoft Corp.</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner kategori world citizenship indonesia</a>
                    </div>
                </div>
                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/itsexpologo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>IT Contest merupakan salah satu kompetisi nasional dari rangkaian acara ITS EXPO</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner kategori pengembangan aplikasi</a>
                    </div>
                </div>

                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/pimnaslogo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>PIMNAS merupakan puncak kegiatan ilmiah mahasiswa berskala nasional yang diselenggarakan oleh DIKTI</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner kategori rancangan perangkat lunak</a>
                    </div>
                </div>
                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/youthlogo.jpg');?>?">
                    </div>
                    <div class="card-content">
                        <p>Kontes internasional yang diselenggarakan oleh the Goal Peace Foundation, Stiftung Enterpreneurship (Berlin) dan UNESCO</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">nominator touth zitizen entrepreneurship competition</a>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/indigologo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>INDIGO Incubator merupakan inkubasi startup yang dimiliki oleh Telkom Indonesia</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner startup kategori health application</a>
                    </div>
                </div>

                 <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/gemastik6logo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>GEMASTIK 6 merupakan pagelaran mahasiswa nasional bidang teknologi informasi k-6 yang diselenggarakan oleh DIKTI</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner kategori rancangan perangkat lunak</a>
                    </div>
                </div>

                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/itslogo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>SNITCH merupakan ajang kompetisi dalam bidang Informatika yang diselenggarakan oleh ITS</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner kategori pengembangan aplikasi</a>
                    </div>
                </div>
                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/mandirilogo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>Mandiri Young Technopreneur merupakan ajang kompetisi bagi wirausaha muda yang memiliki produk berbasis teknologi</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner kategori digital</a>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/lcenlogo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>LCEN XVII adalah lomba cipta elektronik nasional ke-17 yang diselenggarakan oleh ITS.</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner kategori medical electronics & assistive technology</a>
                    </div>
                </div>

                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/gemastik6logo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>GEMASTIK 6 merupakan pagelaran mahasiswa nasional bidang teknologi informasi k-6 yang diselenggarakan oleh DIKTI.</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">winner kategori inovasi perangkat lunak</a>
                    </div>
                </div>
                <div class="card bayangan_2dp">
                    <div class="card-image">
                        <img style="padding: 10% 0;width: 80%; margin-left: auto; margin-right: auto" 
                        src="<?php echo site_url('assets/img/inaictalogo.png');?>?">
                    </div>
                    <div class="card-content">
                        <p>INAICTA merupakan Indonesia ICT Award yang diselenggarakan oleh Kemenkominfo</p>
                    </div>
                    <div class="card-action">
                        <a style="text-transform: capitalize; line-height: ">pecial mention kategori tertiary student project</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>