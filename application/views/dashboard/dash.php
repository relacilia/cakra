		<h4 class="nama-dash">
			Selamat datang, <strong><?php echo $user_profile->name; ?>!</strong>
		</h4>
		<div class="content">
			<div class="row">
				<div class="col s12 m4 l4">
					<?php
						$this->load->view('dashboard/dash_profile', $user_profile);
					?>
				</div>
				<div class="col s12 m8 l8">
					<?php if ($riwayat != NULL) {?>
					<div class="subcol s12">
						<?php $this->load->view('dashboard/pemberitahuan', $riwayat); ?>
					</div>
					<?php } ?>
					<div class="subcol s12">
						<?php $this->load->view('dashboard/dash_notif'); ?>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
