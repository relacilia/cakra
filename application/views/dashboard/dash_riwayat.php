<div class="bg-cakra">
    <div class="nama-content">
        <i class="material-icons left">history</i>
        <span>Riwayat Pemesanan</span>
    </div>
    <div class="isi-content">
        <div class="riwayat">
            <table class="striped">
                <thead>
                    <tr>
                        <th data-field="id">ID</th>
                        <th data-field="name">Produk</th>
                        <th data-field="tanggal">Tanggal Pesan</th>
                        <th data-field="total_harga">Total Harga</th>
                        <th data-field="status">Status</th>
                        <th class="center" data-field="action">Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($riwayat) == 0) { ?>

                    <?php } ?>
                    <?php for($i=0; $i<count($riwayat); $i++) { ?>
                        <tr>
                            <td>#<?php echo $riwayat[$i]->id; ?></td>
                            <td style="text-transform: capitalize">cakra <?php echo $riwayat[$i]->edition; ?></td>
                            <td><?php echo $riwayat[$i]->created; ?></td>
                            <td><?php echo $riwayat[$i]->Biaya_Total; ?></td>
                            <td>
                                <div class="action">
                                    <a class="btn-flat" data-warna="<?php echo $riwayat[$i]->status; ?>">
                                        <?php echo $riwayat[$i]->status; ?>
                                    </a>
                                </div>
                            </td>

                            <td class="center">

                                <div class="action">
                                    <a href="<?php echo base_url(); ?>/dashboard/lihat_detail/<?php echo $riwayat[$i]->id; ?>" class="btn-flat" data-warna="green">
                                        <i class="material-icons left">search</i>
                                        Lihat Detail
                                    </a>
                                    <?php if($riwayat[$i]->status == 'pending') { ?>
                                    <a href="<?php echo base_url(); ?>/dashboard/konfirmasi/<?php echo $riwayat[$i]->id; ?>" class="btn-flat" data-warna="deep-blue" >
                                        <i class="material-icons left">done_all</i>
                                        konfirmasi
                                    </a>

                                    <a href="<?php echo base_url(); ?>/dashboard/hapus/<?php echo $riwayat[$i]->id; ?>" class="btn-flat" data-warna="red">
                                        <i class="material-icons left">close</i>
                                        batal
                                    </a>
                                    <?php }  ?>
                                </div>
                            </td>

                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('template/pop_persetujuan'); ?>
