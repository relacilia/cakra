<div class="bg-cakra">
    <div class="nama-content">
        <i class="material-icons left">notifications</i>
        <span>Pemberitahuan</span>
    </div>
    <div class="isi-content">
        <div class="pemberitahuan">
                <div class="field success">
                  <i>
                      <i class="material-icons left">shopping_cart</i>
                      Cara Pemesanan
                  </i>
                    <div class="isi">
                        <p>Untuk pengalaman pemesanan yang lebih baik, silahkan baca tata caranya
                          <a href="<?php echo base_url()?>dashboard/cara_order"> di sini </a></p>
                    </div>
                      <br />
                    <i>
                        <i class="material-icons">local_phone</i>
                        Contact Person
                    </i>
                    <div class="isi">
                        <p>Jika terdapat pertanyaan/saran/keluhan silahkan hubungi CP
                          <a href="<?php echo base_url()?>dashboard/contact"> di sini </a></p>
                    </div>
                </div>


        </div>
    </div>
</div>
