
		<div class="content">
			<div class="row">
				<p style="font-size: 1.3rem; font-weight:bold; color: rgba(8,64,61, .7); margin: 10px; ">Detail Pemesanan ID: <?php echo $order->id ?> </p>

				<div class="col s12">
					<div class="row">
							<div class="col s6">
								<div class="bg-cakra ">
								<div style="text-align: left; margin: 10px">
								<div class="ringkas">
								<table>
									<tr><td style="width:32%">User_Id</td><td>:</td><td><?php echo $order->webuser_id ?></td></tr>
									<tr><td style="width:32%">Tanggal Pesan</td><td>:</td><td><?php echo $order->created ?></td></tr>
									<tr><td style="width:32%">Jenis </td><td>:</td><td><?php echo $order->edition?></td></tr>
									<tr><td style="width:32%">Jumlah Barang</td><td>:</td><td><?php echo $order->Jumlah_Barang ?></td></tr>
									<tr><td style="width:32%">Biaya Total</td><td>:</td><td><?php echo $order->Biaya_Total ?></td></tr>
									<tr><td style="width:32%">Status</td><td>:</td><td><?php echo $order->status ?> </td></tr>
								</table>
								</div>
								</div>
								</div>
							</div>

							<div class="col s6">
								<div class="bg-cakra ">
								<div style="text-align: left; margin: 10px">
								<div class="ringkas">
								<table>
									<tr><td style="width:32%">Nama Pemesan</td><td>:</td><td><?php echo $order->Nama_Pemesan ?></td></tr>
									<tr><td style="width:32%">Email Pemesan</td><td>:</td><td><?php echo $order->Email_Pemesan ?></td></tr>
									<tr><td style="width:32%">Nomor HP</td><td>:</td><td><?php echo $order->Nomor_HP ?></td></tr>
									<tr><td style="width:32%">Propinsi</td><td>:</td><td><?php echo $order->Provinsi ?></td></tr>
									<tr><td style="width:32%">Kota/Kabupaten</td><td>:</td><td><?php echo $order->Kabupaten ?></td></tr>
									<tr><td style="width:32%">Kecamatan</td><td>:</td><td><?php echo $order->Kecamatan ?></td></tr>
									<tr><td style="width:32%">Alamat</td><td>:</td><td><?php echo $order->Alamat ?></td></tr>
									<tr><td style="width:32%">Kode_Pos</td><td>:</td><td><?php echo $order->Kode_Pos ?></td></tr>
									<tr><td style="width:32%">Jasa Pengiriman</td><td>:</td><td><?php echo $order->Jasa_Pengiriman ?></td></tr>
								</table>
								</div>
								</div>
								</div>
							</div>
					</div>
				</div>
	</div>
	</div>
</section>
