		<div class="content" style="margin-top: -50px">

					    <section id="produk">

	        <div style="background-color: white; margin-top: 80px">
	        	<!-- <h3>Produk Cakra</h3> -->
	            <div class="row">

		        	<div class="col s12 m4 l4">
		        		<div style="width: 100%; padding: 0; margin-bottom: 5px">
		        			<img style="padding: 0 15px; width: 100%;" src="<?php echo site_url('assets/img/cb.svg')?>">
		        		</div>
		        		<div class="edisi-content" style="background-color: rgba(205, 127, 52, .1)">
			        		<!-- <div class="lingkaran-bg" style="border: 1px solid rgba(205, 127, 52, 1)">
				        		<div class="gambar">
						            <i class="material-icons" style="color: rgba(205, 127, 52, 1)">star_border</i>
						        </div>
						    </div> -->
						    <!-- <div class="detail-manfaat" >
							    <h2 style="color: rgba(205, 127, 52, 1)">Cakra Bronze</h2>
							    <p style="color: rgba(205, 127, 52, .8)">Free Version</p>
							</div> -->
							<table class="striped bronze">
	                            <tbody>
	                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Dasar</td>
	                                    <td>10 jenis</td>
	                                </tr>
	                                <tr>
	                                    <td>Tahap Menengah</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Tahap Lanjut</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>

	                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Terintruksi</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Mentoring Bulanan</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Laporan Perkembangan</td>
	                                    <td>4 jenis</td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Pemakaian</td>
	                                    <td>Selamanya</td>
	                                </tr>

	                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Resources</td>
	                                    <td>26</td>
	                                </tr>
	                                <tr>
	                                    <td>Sound/Video</td>
	                                    <td>Tetap</td>
	                                </tr>
	                                <tr>
	                                    <td>Informasi</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Video Tutorial</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>
	                            </tbody>
	                        </table>
							<p style="text-align: center; margin-top: 15px;">
								<?php if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) { ?>
				                <a target="_blank" style=" width: 100%;" href="<?php  echo site_url('dashboard/download'); ?>" class="btn-flat bayangan_2dp" data-warna="deep-blue">Download Gratis</a>
				                <?php } else { ?>
				                	<a style=" width: 100%;" href="<?php  echo site_url('user/download'); ?>" class="btn-flat bayangan_2dp" data-warna="deep-blue">Download Gratis</a>
				                <?php } ?>
				            </p>
						</div>

		        	</div>

		        	<div class="col s12 m4 l4">
		        		<div style="width: 100%; padding: 0; margin-bottom: 5px">
		        			<img style="padding: 0 15px; width: 100%;" src="<?php echo site_url('assets/img/cs.svg')?>">
		        		</div>

		        		<div class="edisi-content" style="background-color: rgba(166, 166, 166, .1)">
			        		<!-- <div class="lingkaran-bg" style="border: 1px solid rgba(166, 166, 166, 1)">
				        		<div class="gambar">
						            <i class="material-icons" style="color: rgba(166, 166, 166, 1)">star_half</i>
						        </div>
						    </div>
						    <div class="detail-manfaat" >
							    <h2 style="color: rgba(166, 166, 166, 1)">Cakra Silver</h2>
							    <p style="color: rgba(166, 166, 166, .8)">Premium Version</p>
							</div> -->

							<table class="striped silver">
	                            <tbody>
	                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Dasar</td>
	                                    <td>38 jenis</td>
	                                </tr>
	                                <tr>
	                                    <td>Tahap Menengah</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Tahap Lanjut</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>

	                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Terintruksi</td>
	                                    <td>1 tahap</td>
	                                </tr>
	                                <tr>
	                                    <td>Mentoring Bulanan</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Laporan Perkembangan</td>
	                                    <td>4 jenis</td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Pemakaian</td>
	                                    <td>Selamanya</td>
	                                </tr>

	                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Resources</td>
	                                    <td>480+</td>
	                                </tr>
	                                <tr>
	                                    <td>Sound/Video</td>
	                                    <td>Berubah</td>
	                                </tr>
	                                <tr>
	                                    <td>Informasi</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Video Tutorial</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                            </tbody>
	                        </table>
							<p style="text-align: center; margin-top: 15px;">
				                <a style=" width: 100%;" href="<?php echo site_url('dashboard/isi_data/SILVER') ?>" class="btn-flat bayangan_2dp" data-warna="cakra">
				                	Beli Sekarang
				                </a>
				            </p>
						</div>

		        	</div>

		        	<div class="col s12 m4 l4">
		        		<div style="width: 100%; padding: 0; margin-bottom: 5px">
		        			<img style="padding: 0 15px; width: 100%;" src="<?php echo site_url('assets/img/cg.svg')?>">
		        		</div>
		        		<div class="edisi-content" style="background-color: rgba(211, 174, 55, .1)">
			        		<!-- <div class="lingkaran-bg" style="border: 1px solid rgba(211, 174, 55, 1)">
				        		<div class="gambar">
						            <i class="material-icons" style="color: rgba(211, 174, 55, 1)">star</i>
						        </div>
						    </div>
						    <div class="detail-manfaat" >
							    <h2 style="color: rgba(211, 174, 55, 1)">Cakra Gold</h2>
							    <p style="color: rgba(211, 174, 55, .8)">Premium Version</p>
							</div> -->
							<table class="striped gold">
	                            <tbody>
	                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Dasar</td>
	                                    <td>38 jenis</td>
	                                </tr>
	                                <tr>
	                                    <td>Tahap Menengah</td>
	                                    <td>56 jenis</td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Tahap Lanjut</td>
	                                    <td>28 jenis</td>
	                                </tr>

	                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Terintruksi</td>
	                                    <td>3 tahap</td>
	                                </tr>
	                                <tr>
	                                    <td>Mentoring Bulanan</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Laporan Perkembangan</td>
	                                    <td>4 jenis</td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Pemakaian</td>
	                                    <td>Selamanya</td>
	                                </tr>

	                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Resources</td>
	                                    <td>1670+</td>
	                                </tr>
	                                <tr>
	                                    <td>Sound/Video</td>
	                                    <td>Berubah</td>
	                                </tr>
	                                <tr>
	                                    <td>Informasi</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Video Tutorial</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                            </tbody>
	                        </table>
							<p style="text-align: center; margin-top: 15px;">
				                <a style="width: 100%;" href="<?php echo site_url('dashboard/isi_data/GOLD') ?>" class="btn-flat bayangan_2dp" data-warna="cakra">
				                	Beli Sekarang
				                </a>
				            </p>
						</div>

		        	</div>

		        </div>
			<!-- <p style="text-align: center; margin: 0">
		        <a style="width: 200px;" href="<?php echo site_url('home/produk') ?>" class="btn-flat bayangan_2dp" data-warna="grey">detail produk...</a>
		    </p> -->
	        </div>
	</section>


		</div>

	</div>
</section>


<!--<?php $this->load->view('dashboard/dash_riwayat'); ?> -->
