<div class="bg-cakra">
	<div class="nama-content">
        	<i class="material-icons left">person</i>
        	<span>Profile</span>
	</div>
	<div class="isi-content">
        <div class="profile">
        	<div class="field">
        		<i>Nama</i>
        		<div class="isi" style="text-transform: capitalize">
        			<?php echo $name?>
        		</div>
        	</div>
        	<div class="field">
        		<i>Nomer HP</i>
        		<div class="isi">
        			<?php echo $phone?>
        		</div>
        	</div>
        	<div class="field">
        		<i>Email</i>
        		<div class="isi alamat">
        			<?php echo $email?>
        		</div>
        	</div>
        	<div class="field">
        		<i>Profesi</i>
        		<div class="isi alamat">
        			<?php echo $profession?>
        		</div>
        	</div>
        	<div class="field">
        		<i>Alamat</i>
        		<div class="isi alamat">
        			<?php echo $address?>
        		</div>
        	</div>
        	<!-- <div class="action">
	            <a href="<?php echo site_url('home/cakra') ?>" class="btn-flat" data-warna="cakra">
	            	<i class="material-icons left">edit</i>Edit
	            </a> 
	            <a href="<?php echo site_url('home/cakra') ?>" class="btn-flat" data-warna="grey">
	            	ubah password
	            </a>
        	</div> -->
        </div>
	</div>
</div>