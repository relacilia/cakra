		<div class="content">
			<div class="row">
				<div class="col s12">
					<div class="col s10" style="margin-left:5%;">
					<div class="bg-cakra ">
					<div style="text-align: left; margin: 10px">
						<p style="font-size: 2rem; font-weight:bold; color: rgba(8,64,61, .7); margin: 10px; text-align: center;">Cara Order </p>

					<div class="row">
					<div class="col s3" style="text-align:center">
						<img src="<?php echo base_url(); ?>assets/img/1.png" style="width:150px; height:150px;">
					</div>
					<div class="col s9" style="line-height:20px;">
						<br>
						<p style="font-size: 1.3rem; font-weight:bold; color:#1ACDB0; margin: 10px;"> 1. Memilih Paket </p>
						<p style="margin-left:10px; text-align:justify;"> Pilih paket yang diinginkan melalui menu <a href="<?php echo base_url()?>dashboard/pesan_paket"><span style="color: #009688;"> Pesan Paket </span></a>. Jika Anda memilih Paket Bronze maka paket dapat langsung Anda download. Akan tetapi, jika Anda memilih paket Silver dan Gold, maka Anda harus mengikuti langkah berikutnya. </p>
					</div>
				    </div>

				    <div class="row">
					<div class="col s3" style="text-align:center">
						<img src="<?php echo base_url(); ?>assets/img/2.png" style="width:150px; height:150px;">
					</div>
					<div class="col s9" style="line-height:20px;">
						<br>
						<p style="font-size: 1.3rem; font-weight:bold; color:#1ACDB0; margin: 10px;"> 2. Mengisi Data Pemesan </p>
						<p style="margin-left:10px; text-align:justify;"> Isi data Anda/Pemesan dengan benar. Periksa kembali data yang telah Anda masukkan sebelum Anda kirim. Jika ada kesalahan maka akan menyebabkan masalah pada pengiriman barang.</p>
					</div>
				    </div>

				    <div class="row">
					<div class="col s3" style="text-align:center">
						<img src="<?php echo base_url(); ?>assets/img/3.png" style="width:150px; height:150px;">
					</div>
					<div class="col s9" style="line-height:20px;">
						<br>
						<p style="font-size: 1.3rem; font-weight:bold; color:#1ACDB0; margin: 10px;"> 3. Melakukan Pembayaran </p>
						<p style="margin-left:10px; text-align:justify;"> Lakukan pembayaran ke Bank <strong><?php echo $contact->nama_bank ?></strong> dengan No. Rekening <strong><?php echo $contact->no_rekening ?></strong> atas nama <strong><?php echo $contact->nama_rekening ?></strong> sejumlah harga yang tertera pada Riwayat Pemesanan.</p>
					</div>
				    </div>

				    <div class="row">
					<div class="col s3" style="text-align:center">
						<img src="<?php echo base_url(); ?>assets/img/4.png" style="width:150px; height:150px;">
					</div>
					<div class="col s9" style="line-height:20px;">
						<br>
						<p style="font-size: 1.3rem; font-weight:bold; color:#1ACDB0; margin: 10px;"> 4. Melakukan Konfirmasi </p>
						<p style="margin-left:10px; text-align:justify;">Jika Anda sudah melakukan pembayaran, segera lakukan konfirmasi dengan cara mengunggah bukti pembayaran Anda di menu Riwayat Pemesanan atau bisa juga dengan
							menghubungi Contact Person di menu Kontak. Barang Anda tidak akan diproses sebelum Anda melakukan konfirmasi.</p>
					</div>
				    </div>

				     <div class="row">
					<div class="col s3" style="text-align:center">
						<img src="<?php echo base_url(); ?>assets/img/5.png" style="width:150px; height:150px;">
					</div>
					<div class="col s9" style="line-height:20px;">
						<br>
						<p style="font-size: 1.3rem; font-weight:bold; color:#1ACDB0; margin: 10px;"> 5. Paket akan dikirim </p>
						<p style="margin-left:10px; text-align:justify;"> Paket akan dikirim ke alamat Anda/Pemesan tidak lama setelah konfirmasi pembayaran.</p>
					</div>
				    </div>



	                </div>
	            </div>
	        </div>





				</div>
			</div>
		</div>

	</div>
</section>


<!--<?php $this->load->view('dashboard/dash_riwayat'); ?> -->
