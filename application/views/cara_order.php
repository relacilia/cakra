<section id="cara_order">
    <div class="pernyataan" style="margin: 0">
        <h5 style="font-size: 2.5rem; font-weight: 200;">Cara Order</h5>
        <p style="margin-bottom: 30px; font-size: 1.2rem; font-weight: 400">Untuk <strong>Cakra Silver</strong> dan <strong>Cakra Gold</strong></p>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12" style="margin-bottom: 30px;">
                    <div class="lingkaran-bg">
                        <div class="gambar">
                            <i class="material-icons">assignment_turned_in</i>
                        </div>
                    </div>
                    <div class="detail-manfaat">
                        <h4 style="margin-bottom: 15px;">1. Login</h4>
                        <p style="color: rgba(255,255,255, .8); line-height: 20px">
                            Pastikan Anda sudah memilih akun untuk login ke sistem <strong>Cakra</strong>. <a href="<?php echo site_url('user')?>">Login</a> atau <a href="<?php echo site_url('user/daftar')?>">Daftar</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
