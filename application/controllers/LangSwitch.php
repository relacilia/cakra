<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LangSwitch extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    public function switchLanguage($language = "") {
        $language = ($language != "") ? $language : "indo";
        $this->session->set_userdata('site_lang', $language);
        redirect_back();
    }
}