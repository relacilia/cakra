<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_Admin extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->is_login()){
            redirect("user");
        }

        $this->load->model('user_model');
        $this->load->model('order_model');
				$this->load->model('Complement_model');
         $this->load->model('Admin_model','',TRUE);
    }

    public function index()
	{
		// $data['page_title'] = 'Home';
		$data['menu_aktif'] = 'dash';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard Admin";
		$data['path_']          = array( 0 => 'Home' );


		$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);
		$data['riwayat']	  = $this->order_model->get_pesanan_verifikasi();

		if($_SESSION['admin'] == 'admin')
		{
			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/dasha', $data);
			$this->load->view('dashboard/footer', $data);
		}
		else
		{
			redirect("dashboard");
		}

	}

	public function list_pesan($id=NULL)
	{

		if($_SESSION['admin'] == 'admin')
		{

			$data['menu_aktif'] = 'list_pesan';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard Admin";
			$data['path_']          = array( 0 => 'List Pemesanan' );

			$jumlah = $this->order_model->jumlah();
			$config['base_url'] = base_url().'Dashboard_Admin/list_pesan/';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 10;
			$config['first_page'] = 'Awal';
			 $config['last_page'] = 'Akhir';
			 $config['next_page'] = '&laquo;';
			 $config['prev_page'] = '&raquo;';



			$data['query'] = $this->order_model->lihat($config['per_page'],$id);
			$this->pagination->initialize($config);
			$data['halaman'] = $this->pagination->create_links();



			$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);


			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/list_pesan', $data);
			$this->load->view('dashboard/footer', $data);
		}
		else
		{
			redirect("dashboard");
		}


	}

	public function hapus($id)
	{
		$this->order_model->delete_order($id);
		 redirect('dashboard_admin/list_pesan');
	}

	public function hapus_akun($id)
	{
		$this->user_model->delete_akun($id);
		 redirect('dashboard_admin/list_akun');
	}

	public function list_akun($id=NULL)
	{

		if($_SESSION['admin'] == 'admin')
		{

			$data['menu_aktif'] = 'list_akun';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard Admin";
			$data['path_']          = array( 0 => 'List Akun' );

			$jumlah = $this->user_model->jumlah();
			$config['base_url'] = base_url().'Dashboard_admin/list_akun/';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 10;
			$config['first_page'] = 'Awal';
			 $config['last_page'] = 'Akhir';
			 $config['next_page'] = '&laquo;';
			 $config['prev_page'] = '&raquo;';



			$data['query'] = $this->user_model->lihat_akun($config['per_page'],$id);
			$this->pagination->initialize($config);
			$data['halaman'] = $this->pagination->create_links();


			$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);
			$data['akun'] = $this->user_model->get_all_user();

			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/list_akun', $data);
			$this->load->view('dashboard/footer', $data);
		}
		else
		{
			redirect("dashboard");
		}


	}

	public function list_admin()
	{

		if($_SESSION['admin'] == 'admin')
		{

			$data['menu_aktif'] = 'list_akun';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard Admin";
			$data['path_']          = array( 0 => 'List Admin' );


			$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);
			$data['akun'] = $this->Admin_model->get_all_admin();

			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/list_admin', $data);
			$this->load->view('dashboard/footer', $data);
		}
		else
		{
			redirect("dashboard");
		}


	}

	public function submit_admin(){
		if($_SESSION['admin'] == 'admin')
		{

			$nama 	= $this->input->post('nama');
			$username 		= $this->input->post('uname');
			$password 	= $this->input->post('password');

			if($this->Admin_model->create_user($nama, $username, $password)) {
				$this->session->set_flashdata('pesan','<strong>Selamat</strong> akun Anda berhasil diaktifkan!');
				$this->session->set_flashdata('tipe', 'success');
				redirect('dashboard_admin/list_admin');
			} else {
				$this->session->set_flashdata('pesan','<strong>Gagal!</strong> username Anda sudah terdaftar');
				$this->session->set_flashdata('tipe', 'error');
				redirect('dashboard_admin/list_admin');
		}

		}
		else
		{
			redirect("dashboard");
		}
	}

	public function lihat_detail($id){
		if($_SESSION['admin'] == 'admin')
		{
			$data['menu_aktif'] = 'Detail_Pemesanan';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard Admin";
			$data['path_']          = array( 0 => 'Detail Pemesanan' );


			$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);

			$data['order'] = $this->order_model->get_detail($id);
			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/edit_status', $data);
			$this->load->view('dashboard/footer', $data);
		}


		else
		{
			redirect("dashboard");
		}
	}

	public function edit_status($id)
	{
		if($_SESSION['admin'] == 'admin')
		{
			$data = array(
				'status' => $this->input->post('status')
				);
			$data['status']=$this->order_model->edit_status($id,$data);

			$data['menu_aktif'] = 'Detail_Pemesanan';
			$data['no_footer'] = true;

			$data['nama_page'] = "Detail Pemesanan";
			$data['path_']          = array( 0 => 'Home' );


			$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);

			$data['order'] = $this->order_model->get_detail($id);
			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/edit_status', $data);
			$this->load->view('dashboard/footer', $data);
		}


		else
		{
			redirect("dashboard");
		}
	}

	public function pesan_paket() {

		$data['menu_aktif'] = 'pesan_paket';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard";
		$data['path_']          = array( 0 => 'Pesan Paket' );

		if($_SESSION['admin'] == 'admin')
		{
			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/pesan_paket', $data);
			$this->load->view('dashboard/footer', $data);
		}

		else
		{
			redirect("dashboard");
		}

	}

	public function isi_data($tipe) {

		if($_SESSION['admin'] == 'admin')
		{
			$data['menu_aktif'] = 'isi_data';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard";
			$data['path_']          = array( 0 => 'Pesan Paket', 1=> 'Isi data' );
			$data['provinsi']=$this->order_model->ambil_provinsi();
			$data['tipe'] = $tipe;
			$data['harga'] =$this->order_model->get_harga($tipe);


			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/isi_data', $data);
			$this->load->view('dashboard/footer', $data);
		}

		else
		{
			redirect("dashboard");
		}

	}

	public function add_order($tipe)
	{
		$prov['query'] = $this->order_model->nama_prov($this->input->post('provinsi_id'));
		$kab['query'] = $this->order_model->nama_kab($this->input->post('kabupaten_id'));
		$kec['query'] = $this->order_model->nama_kec($this->input->post('kecamatan_id'));
		$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);
		$data['tipe'] = $tipe;
		$data['harga'] =$this->order_model->get_harga($tipe);

		$data =  array(
		'webuser_id'   		=> '10'.$data['user_profile']->id,
		'created' 			=> date('Y-m-j H:i:s'),
		'status' => 'pending',
		'edition' => $data['tipe'],
		'Nama_Pemesan' => $this->input->post('name'),
		'Email_Pemesan' => $this->input->post('email'),
		'Nomor_HP' => $this->input->post('nomor'),
		'Provinsi'=>$prov['query'],
		'Kabupaten'=>$kab['query'],
		'Kecamatan'=>$kec['query'],
		'Alamat' => $this->input->post('alamat'),
		'Jasa_Pengiriman' => $this->input->post('jasa'),
		'Kode_Pos' => $this->input->post('kode'),
		'Jumlah_Barang' => $this->input->post('jumlah'),
		'Biaya_Total' => $data['harga'] * $this->input->post('jumlah')
		);

		$res = $this->order_model->simpan($data);

		if($res >= 1)
		{
			redirect('dashboard_admin/isi_data/SILVER');
		}

		else
		{
			redirect('dashboard_admin/list_pesan');
		}
	}

	public function search($id=NULL)
	{

		if($_SESSION['admin'] == 'admin')
		{

			$data['menu_aktif'] = 'list_pesan';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard Admin";
			$data['path_']          = array( 0 => 'List Pemesanan' );



			$find = $this->input->post('find');
			$data['query'] = $this->order_model->lihatsearch($find);



			$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);


			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/list_search', $data);
			$this->load->view('dashboard/footer', $data);
		}
		else
		{
			redirect("dashboard");
		}
	}

	public function u_kontak ()
	{

		if($_SESSION['admin'] == 'admin')
		{

			$data['menu_aktif'] = 'edit_kontak';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard Admin";
			$data['path_']          = array( 0 => 'Edit Kontak' );
			$data['contact'] = $this->Complement_model->get_contact();
			$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);


			$this->load->view('template/header', $data);
			$this->load->view('dashboard_admin/menu_admin', $data);
			$this->load->view('dashboard_admin/edit_kontak', $data);
			$this->load->view('dashboard/footer', $data);
		}
		else
		{
			redirect("dashboard");
		}
	}

	public function ubah_kontak ()
	{

		if($_SESSION['admin'] == 'admin')
		{

			$data['menu_aktif'] = 'edit_kontak';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard Admin";
			$data['path_']          = array( 0 => 'Edit Kontak' );
			$data['contact'] = $this->Complement_model->get_contact();
			$data['user_profile'] = $this->Admin_model->get_user($_SESSION['username']);

			$arr = array(
			'no_cp' => $this->input->post('no_cp'),
			'nama_cp' => $this->input->post('nama_cp'),
			'nama_bank' =>  $this->input->post('nama_bank'),
			'no_rekening' => $this->input->post('no_rekening'),
			'nama_rekening' => $this->input->post('nama_rekening'),
			'email' => $this->input->post('email')
			);

			$data['contact'] = $this->Complement_model->update($arr);

			redirect('dashboard_admin/u_kontak');

		}
		else
		{
			redirect("dashboard");
		}
	}
}
