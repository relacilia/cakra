<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->is_login()){
            redirect("user");
        }

        $this->load->model('user_model');
        $this->load->model('order_model');
				$this->load->model('Complement_model');
         $this->load->model('Admin_model','',TRUE);
    }

    // dashboard
	public function index()
	{
		$data['menu_aktif'] = 'dash';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard";
		$data['url'] = array (0 => '<a href="dashboard">');
		$data['path_']          = array( 0 => 'Home </a>' );


		$data['user_profile'] = $this->user_model->get_user($_SESSION['username']);
		$data['riwayat']	  = $this->order_model->get_pesanan_konfirmasi($data['user_profile']->id);
		// var_dump($data['riwayat']);
		// break;

		if($_SESSION['admin'] == 'user')
		{
			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/dash', $data);
			$this->load->view('dashboard/footer', $data);
		}

		else
		{
			redirect("dashboard_admin");
		}
	}

	public function download() {
		redirect('dashboard/download_file', 'refresh');
	}

	public function download_file() {
		$this->load->helper('download');
		if($this->user_model->verify_download($_SESSION['username'], $_SESSION['kode'])) {
			$data = file_get_contents(base_url()."assets/cakra_bronze_v1.zip");
			$name = 'CAKRA_Bronze_Setup.zip';

			force_download($name, $data);
			redirect('dashboard');
		}
	}

	//riwayat
	public function riwayat() {
		$data['menu_aktif'] = 'riwayat';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard";
		$data['url'] = array (0 => '<a href="riwayat">');
		$data['path_']          = array( 0 => 'Riwayat Pemesanan </a>' );

		$data['user_profile'] = $this->user_model->get_user($_SESSION['username']);
		$data['riwayat']	  = $this->order_model->get_all_pesanan_user($data['user_profile']->id);
		$data['contact'] = $this->Complement_model->get_contact();


		if($_SESSION['admin'] == 'user')
		{
			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/riwayat', $data);
			$this->load->view('dashboard/footer', $data);
		}

		else
		{
			redirect("dashboard_admin");
		}

	}

	public function cara_order() {
		$data['menu_aktif'] = 'cara_order';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard";
		$data['url'] = array (0 => '<a href="cara_order">');
		$data['path_']          = array( 0 => 'Cara Order </a>' );
		$data['contact'] = $this->Complement_model->get_contact();

		if($_SESSION['admin'] == 'user')
		{
			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/cara_order', $data);
			$this->load->view('dashboard/footer', $data);
		}

		else
		{
			redirect("dashboard_admin");
		}

	}

	public function pesan_paket() {
		$data['menu_aktif'] = 'pesan_paket';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard";
		$data['url'] = array (0 => '<a href="pesan_paket">');
		$data['path_']          = array( 0 => 'Pesan Paket </a>' );

		if($_SESSION['admin'] == 'user')
		{
			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/pesan_paket', $data);
			$this->load->view('dashboard/footer', $data);
		}

		else
		{
			redirect("dashboard_admin");
		}

	}

	public function isi_data($tipe) {

		if($_SESSION['admin'] == 'user')
		{
			$data['menu_aktif'] = 'isi_data';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard";
			$data['url'] = array (0 => '', 1 => '');
			$data['path_']          = array( 0 => 'Pesan Paket </a>', 1 => 'Isi Data </a>' );
			$data['provinsi']=$this->order_model->ambil_provinsi();
			$data['tipe'] = $tipe;
			$data['harga'] =$this->order_model->get_harga($tipe);


			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/isi_data', $data);
			$this->load->view('dashboard/footer', $data);
		}

		else
		{
			redirect("dashboard_admin");
		}

	}

	// public function review() {
	// 	$data['menu_aktif'] = 'review';
	// 	$data['no_footer'] = true;

	// 	$data['nama_page'] = "Dashboard";
	// 	$data['path_']          = array( 0 => 'Review' );

	// 	$this->load->view('template/header', $data);
	// 	$this->load->view('dashboard/menu', $data);
	// 	$this->load->view('dashboard/ringkasan', $data);
	// 	$this->load->view('dashboard/footer', $data);
	// }

	public function konfirmasi($id) {
		if($_SESSION['admin'] == 'user')
		{
			$data['menu_aktif'] = 'konfirmasi';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard";
			$data['url'] = array (0 => '<a href="konfirmasi">');
			$data['path_']          = array( 0 => 'konfirmasi </a>' );
			$data['contact'] = $this->Complement_model->get_contact();
			$data['id'] = $id;

			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/konfirmasi', $data);
			$this->load->view('dashboard/footer', $data);
			}

		else
		{
			redirect("dashboard_admin");
		}


	}

	public function logout() {
		if($this->is_login()) {
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
        }
        redirect('user');
	}

	// dijalankan saat provinsi di klik
	public function pilih_kabupaten(){
		$data['kabupaten']=$this->order_model->ambil_kabupaten($this->uri->segment(3));
		$this->load->view('dashboard/v_drop_down_kabupaten',$data);
	}

	// dijalankan saat kabupaten di klik
	public function pilih_kecamatan(){
		$data['kecamatan']=$this->order_model->ambil_kecamatan($this->uri->segment(3));
		$this->load->view('dashboard/v_drop_down_kecamatan',$data);
	}

	// dijalankan saat kecamatan di klik
	public function pilih_kelurahan(){
		$data['kelurahan']=$this->order_model->ambil_kelurahan($this->uri->segment(3));
		$this->load->view('dashboard/v_drop_down_kelurahan',$data);
	}


	public function add_order($tipe)
	{
		$prov['query'] = $this->order_model->nama_prov($this->input->post('provinsi_id'));
		$kab['query'] = $this->order_model->nama_kab($this->input->post('kabupaten_id'));
		$kec['query'] = $this->order_model->nama_kec($this->input->post('kecamatan_id'));
		$data['user_profile'] = $this->user_model->get_user_id($_SESSION['username']);
		$data['tipe'] = $tipe;
		$data['harga'] =$this->order_model->get_harga($tipe);

		$data =  array(
		'webuser_id'   		=> $data['user_profile'],
		'created' 			=> date('Y-m-j H:i:s'),
		'status' => 'pending',
		'edition' => $data['tipe'],
		'Nama_Pemesan' => $this->input->post('name'),
		'Email_Pemesan' => $this->input->post('email'),
		'Nomor_HP' => $this->input->post('nomor'),
		'Provinsi'=>$prov['query'],
		'Kabupaten'=>$kab['query'],
		'Kecamatan'=>$kec['query'],
		'Alamat' => $this->input->post('alamat'),
		'Jasa_Pengiriman' => $this->input->post('jasa'),
		'Kode_Pos' => $this->input->post('kode'),
		'Jumlah_Barang' => $this->input->post('jumlah'),
		'Biaya_Total' => $data['harga'] * $this->input->post('jumlah')
		);

		var_dump($data);
		$res = $this->order_model->simpan($data);

		if($res >= 1)
		{
			redirect('dashboard/isi_data/SILVER');
		}

		else
		{
			redirect('dashboard/riwayat');
		}
	}

	public function kirim($id)
	{

    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '1000';
    // $config['max_width']  = '';
    // $config['max_height']  = '';
    $config['overwrite'] = TRUE;
    $config['remove_spaces'] = TRUE;

    $this->load->library('upload', $config);



    $this->upload->do_upload('bukti');

    if ( ! $this->upload->do_upload('bukti'))
    {
        $error = array('error' => $this->upload->display_errors());

       redirect('dashboard/');
    }
    else
    {
    	$gbr = $this->upload->data();
    	$data = array(
    		'status' => 'proses',
    		'photo' => $gbr['full_path'],
				'nama_photo' => $gbr['file_name']
    		);

    	var_dump($data);


    	$this->order_model->konfirm_q($id,$data);


        redirect('dashboard/riwayat');
    	}
	}

	public function hapus($id)
	{
		$this->order_model->delete_order($id);
		 redirect('dashboard/riwayat');
	}

	public function contact() {
		if($_SESSION['admin'] == 'user')
		{
			$data['menu_aktif'] = 'contact';
			$data['no_footer'] = true;

			$data['nama_page'] = "Dashboard";
			$data['url'] = array (0 => '<a href="contact">');
			$data['path_']          = array( 0 => 'Contact </a>' );
			$data['contact'] = $this->Complement_model->get_contact();
			$data['id'] = $id;

			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/contact', $data);
			$this->load->view('dashboard/footer', $data);
			}

		else
		{
			redirect("dashboard_admin");
		}


	}

	public function lihat_detail($id){
		if($_SESSION['admin'] == 'user')
		{
			$data['menu_aktif'] = 'Detail_Pemesanan';
			$data['no_footer'] = true;

			$data['nama_page'] = "Detail Pemesanan";
			$data['url'] = array (0 => '', 1=> '');
			$data['path_']          = array( 0 => 'Riwayat Pemesanan', 1=> "Lihat Detail" );


			$data['user_profile'] = $this->user_model->get_user($_SESSION['username']);

			$data['order'] = $this->order_model->get_detail($id);
			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/lihat_detail', $data);
			$this->load->view('dashboard/footer', $data);
		}


		else
		{
			redirect("dashboard_admin");
		}
	}

	public function u_profil(){
		if($_SESSION['admin'] == 'user')
		{
			$data['menu_aktif'] = 'Ubah_Profil';
			$data['no_footer'] = true;

			$data['nama_page'] = "Ubah Profil";
			$data['url'] = array (0 => '<a href="u_profil">');
			$data['path_']          = array( 0 => 'Home </a>' );


			$data['q'] = $this->user_model->get_user($_SESSION['username']);

			$this->load->view('template/header', $data);
			$this->load->view('dashboard/menu', $data);
			$this->load->view('template/breadcrumbs', $data);
			$this->load->view('dashboard/profile', $data);
			$this->load->view('dashboard/footer', $data);
		}


		else
		{
			redirect("dashboard_admin");
		}
	}

	public function ubah_profil(){
		if($_SESSION['admin'] == 'user')
		{
			$data['menu_aktif'] = 'Ubah_Profil';
			$data['no_footer'] = true;

			$data['nama_page'] = "Ubah Profil";
			$data['url'] = array (0 => '<a href="ubah_profil">');
			$data['path_']          = array( 0 => 'Home </a>' );


			$data['user_profile'] = $this->user_model->get_user($_SESSION['username']);
			$find = $this->input->post('find');

			$pass1 = $this->input->post('pass');
			$pass2 = $this->input->post('cpass');

			if ($pass1 == $pass2)
			{
				$arr = array(
				'name' => $this->input->post('nama'),
				'address' => $this->input->post('alamat'),
				'password' => $pass1,
				'phone' => $this->input->post('nomor'),
				'profession' => $this->input->post('prof')
			);

			$data['q'] = $this->user_model->update_user($data['user_profile']->email,$arr);

			redirect("dashboard");

			}

		}


		else
		{
			redirect("dashboard_admin");
		}
	}

}
