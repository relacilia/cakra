<?php

// menu
$lang["tentang"] 	= "tentang";
$lang["fitur"]	 	= "fitur";
$lang["tim"] 		= "tim";
$lang["kontak"] 	= "kontak";
$lang["blog"] 		= "blog";
$lang["download"] 	= "download";
$lang["pesan"]		= "pesan";
$lang["masuk"] 		= "masuk";
$lang["bhs_indo"] 	= "bahasa indonesia";
$lang["bhs_ing"] 	= "bahasa inggris";
$lang["daftar"]		= "daftar";
$lang["keluar"]		= "keluar";

$lang["pass"]		= "Kata Sandi";
$lang["error_msg"]	= "Email dan password yang anda masukkan tidak cocok.";
$lang["error_update"] = "Maaf. Data Anda gagal diperbarui";
$lang["berhasil_daftar"] = "Selamat. Anda berhasil mendaftar. Silahkan login untuk mendownload cakra app";
$lang["berhasil_update"] = "Data Anda berhasil diperbarui.";
$lang["berhasil_pesan"] = "Terima Kasih telah mememesan produk kami. Status pemesanan dapat dilihat di Halaman Pemesanan Saya.";

$lang["homepage"]	= "home";
$lang["nama_leng"]  = "Nama Lengkap";
$lang["batal"]		= "batal";

$lang["login_dulu"] = "masuk terlebih dahulu";

// tentang cakra
$lang["ttg_cakra"] 		= "tentang cakra";
$lang["des_ttg_cakra"] 	= "Cakra adalah aplikasi terapi autis dengan teknologi yang interaktif. dengan cakra, orang tua dapat melakukan terapi sendiri di rumah. dengan tampilan yang menarik dan mudah dipahami, proses terapi akan menjadi lebih mudah dan menyenangkan. dikembangkan bersama pakar autis, fitur dalama aplikasi cakra sudah tervalidasi. dilengkapi dengan evaluasi dan laporan sehingga membuat orangtua mudah melihat perkembangan terapi anak.";
$lang["intensif"] 		= "intensif";
$lang["dekat"] 			= "dekat";
$lang["teknologi"] 		= "teknologi";
$lang["des_intensif"] 	= "Aplikasi cakra membantu anda mengintensifkan terapi di rumah, sehingga dapat meningkatkan perkembangan kemampuan anak";
$lang["des_dekat"] 		= "Aplikasi cakra dapat menciptakan rumah sebagai tempat terapi terdekat, sehingga dapat melatih adaptasi dan sosialisasi anak";
$lang["des_teknologi"] 	= "Menggunakan teknologi sebagai stimulus untuk mempercepat perkembangan motorik, bicara, dan akademik anak berkebutuhan khusus";



// fitur
$lang["evaluasi"] 		= "evaluasi";
$lang["eval_1"] 		= "Sesuai dengan standar atec (autism treatment evaluation checklist)";
$lang["eval_2"] 		= "Dikembangkan oleh autism research institute";
$lang["eval_3"] 		= "Mengetahui berapa persen aspek anak";
$lang["eval_4"] 		= "Mengetahui perkembangan anak perbulan";
$lang["terapi"] 		= "terapi";
$lang["terapi_1"] 		= "Terdiri dari dua model: free mode dan terstruktur";
$lang["terapi_2"] 		= "Terapi lengkap untuk autis (tahap dasar, menengah, dan lanjut)";
$lang["terapi_3"] 		= "Menggunakan metode aba yang dikeluarkan lovaas";
$lang["terapi_4"] 		= "Terdiri dari 3 level, 22 kategori, 122 macam terapi dan ribuan konten";
$lang["laporan"] 		= "laporan";
$lang["lap_1"] 			= "Laporan total: rekap dari semua aspek laporan";
$lang["lap_2"] 			= "Laporan per aspek: rincian tiap aspek, meliputi komunikasi, kognitif, sosial dan kebiasan";
$lang["lap_3"] 			= "Laporan bulanan: rekap perbulan dari semua aspek progress ketercapaian tiap materi terapi";

// edisi cakra
$lang["edisi_cakra"] 	= "edisi cakra";
$lang["tahapan_terapi"] = "Tahapan terapi";
$lang["thp_dasar"] 		= "Tahap dasar";
$lang["thp_menengah"] 	= "Tahap menengah";
$lang["thp_lanjut"] 	= "Tahap lanjut";
$lang["fasilitas"] 		= "fasilitas";
$lang["fas_terapi_int"] = "Terapi terintruksi";
$lang["fas_mentoring"] 	= "Mentoring bulanan";
$lang["fas_laporan"] 	= "Laporan perkembangan";
$lang["fas_pemakaian"] 	= "Pemakaian";
$lang["konten"] 		= "konten";
$lang["kon_resource"] 	= "Resource";
$lang["kon_sound_video"]= "Sound / Video Reinforce";
$lang["kon_info"] 		= "Informasi";
$lang["kon_vid_tutor"] 	= "Video tutorial";
$lang["jenis"] 			= "jenis";
$lang["tersedia"] 		= "tersedia";
$lang["selamanya"]		= "selamanya";
$lang["buah"] 			= "buah";
$lang["bisa_berubah"] 	= "bisa berubah";
$lang["tahap"] 			= "tahap";
$lang["tetap"]			= "tetap";

// penghargaan
$lang["penghargaan"] 		= "penghargaan";
$lang["peng_imgcup"]		= "winner kategori world citizenship indonesia";
$lang["peng_imgcup2"]		= "semifinalist kategori world citizenship international";
$lang["despeng_imgcup"] 	= "ImagineCup merupakan kompetisi terbesar yang diadakan oleh Microsoft Corp.";

$lang["peng_indigo"] 		= "winner startup kategori health application";
$lang["despeng_indigo"] 	= "INDIGO Incubator merupakan inkubasi startup yang dimiliki oleh Telkom Indonesia";

$lang["peng_lcen"] 			= "winner kategori medical electronics & assistive technology";
$lang["despeng_lcen"] 		= "LCEN XVII adalah lomba cipta elektronik nasional ke-17 yang diselenggarakan oleh ITS";

$lang["peng_gem_ipl"] 		= "winner kategori inovasi perangkat lunak";
$lang["despeng_gem_ipl"] 	= "GEMASTIK 6 merupakan pagelaran mahasiswa nasional bidang teknologi informasi k-6 yang diselenggarakan oleh DIKTI";

$lang["peng_gem_rpl"] 		= "winner kategori rancangan perangkat lunak";
$lang["despeng_gem_rpl"] 	= "GEMASTIK 6 merupakan pagelaran mahasiswa nasional bidang teknologi informasi k-6 yang diselenggarakan oleh DIKTI";

$lang["peng_itsexpo"] 		= "winner kategori pengembangan aplikasi";
$lang["despeng_itsexpo"] 	= "IT Contest merupakan salah satu kompetisi nasional dari rangkaian acara ITS EXPO";

$lang["peng_inaicta"] 		= "pecial mention kategori tertiary student project";
$lang["despeng_inaicta"] 	= "INAICTA merupakan Indonesia ICT Award yang diselenggarakan oleh Kemenkominfo";

$lang["peng_snitch"] 		= "winner kategori pengembangan aplikasi";
$lang["despeng_snitch"] 	= "SNITCH merupakan ajang kompetisi dalam bidang Informatika yang diselenggarakan oleh ITS";

$lang["peng_pimnas"] 		= "winner kategori rancangan perangkat lunak";
$lang["despeng_pimnas"] 	= "PIMNAS merupakan puncak kegiatan ilmiah mahasiswa berskala nasional yang diselenggarakan oleh DIKTI";

$lang["peng_mandiri"] 		= "winner kategori digital";
$lang["despeng_mandiri"] 	= "Mandiri Young Technopreneur merupakan ajang kompetisi bagi wirausaha muda yang memiliki produk berbasis teknologi";

$lang["peng_nomi"] 			= "nominator touth zitizen entrepreneurship competition";
$lang["despeng_nominator"] 	= "Kontes internasional yang diselenggarakan oleh the Goal Peace Foundation, Stiftung Enterpreneurship (Berlin) dan UNESCO";


// kata mereka
$lang["kata_mereka"] = "kata mereka";

$lang["kata_illy"] 	= "cakra adalah aplikasi yang lengkap dan sesuai untuk penyandang autis.";
$lang["illy"] 		= "hj. illy yudiono";
$lang["jab_illy"] 	= "pakar autis dan pemilik cakra autism center, surabaya";

$lang["kata_fitri"] = "aplikasi yang sangat bagus. kita belum pernah menggunakan aplikasi seperti ini.";
$lang["fitri"] 		= "fitriani";
$lang["jab_fitri"] 	= "guru di tempat terapi";

$lang["kata_yudi"] 	= "cakra dapat bersaing dengan aplikasi lain yang sudah beredar lama di pasaran.";
$lang["yudi"] 		= "h. yudiono";
$lang["jab_yudi"] 	= "pemilik yayasan autis";

$lang["kata_fatir"] = "bagus.";
$lang["fatir"] 		= "fatir";
$lang["jab_fatir"] 	= "anak penyandang autis";

$lang["kata_lolly"] = "cakra adalah aplikasi yang sangat berpotensi.";
$lang["lolly"] 		= "ir. lolly amalia abdullah, m.sc.";
$lang["jab_lolly"] 	= "kepala pusat litbang aplikasi informatika dan ikp kementrian pariwisata dan ekonomi kreatif";

$lang["kata_dian"] 	= "cakra, aplikasi terapi autis terlengkap yang saya temui, dan sangat bermanfaat.";
$lang["dian"] 		= "andreas diantoro";
$lang["jab_dian"] 	= "president director of microsoft indonesia";



$lang["tim_kami"] 			= "tim kami";
$lang["tombol_download"] 	= "download sekarang";
$lang["tombol_pesan"] 		= "pesan sekarang";


// kontak
$lang["kediaman"] 	= "kediaman kami";
$lang["alamat"] 	= "Alamat";
$lang["telp"] 		= "Telp.";
$lang["email"] 		= "Email";
$lang["jam_kerja"] 	= "jam kerja";
$lang["hari_1"] 	= "senin - jum'at";
$lang["jam_1"] 		= "09.00 - 18.00";
$lang["hari_2"] 	= "sabtu - minggu";
$lang["jam_2"] 		= "tutup";
$lang["masukan"] 	= "berikan masukkan";
$lang["nama"] 		= "nama";
$lang["komentar"] 	= "Komentar";
$lang["kirim"] 		= "kirim";


// footer
$lang["disponsori"] 	= "disponsori oleh :";
$lang["dibuat"] 		= "dibuat oleh :";

$lang["perbarui"] 		= "update";

$lang["newpass"]		= "kata sandi baru";